package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.NewneboardApplicationTests;
import me.neboard.newneboard.exception.BusinessException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@AutoConfigureTestEntityManager
@SpringBootTest
public class UrlViewerTest extends NewneboardApplicationTests {
    @Autowired
    private UrlViewer urlViewer;

    @Test
    public void getDomain() throws Exception {
        assertEquals("en.wikipedia.org",
                urlViewer.getDomain("http://en.wikipedia.org/wiki/Main_Page"));
        assertEquals("music.youtube.com",
                urlViewer.getDomain("https://music.youtube.com/watch?v=0SEu8HsZayw"));
        assertEquals("bugreports.qt.io",
                urlViewer.getDomain("https://bugreports.qt.io/browse/QTBUG-28979?jql=text ~ \"wayland\""));
        try {
            urlViewer.getDomain("Not a URL");
            fail("Exception should be thrown on invalid URL");
        } catch (BusinessException e) {
            // This is ok
        }
    }

}
