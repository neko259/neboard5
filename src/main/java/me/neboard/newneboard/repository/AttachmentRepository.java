package me.neboard.newneboard.repository;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
	int countByPostAttachmentsPostThread(PostThread thread);
	List<Attachment> findDistinctByPostAttachmentsPostThread(PostThread thread);
	Attachment findByUrl(String url);
	Attachment findByFilename(String filename);
	List<Attachment> findByUrlNull();

	@Query("SELECT a FROM Attachment a WHERE NOT EXISTS" +
			" (SELECT pa FROM PostAttachment pa WHERE pa.attachment.id = a.id)")
	@Modifying
	List<Attachment> findWithoutPosts();
}
