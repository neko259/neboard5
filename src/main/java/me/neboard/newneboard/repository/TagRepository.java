package me.neboard.newneboard.repository;

import me.neboard.newneboard.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Tag findByName(String name);
    Page<Tag> findByNameContains(Pageable pageable, String name);
    Set<Tag> findByNameIn(Collection<String> names);
    List<Tag> findByNameStartsWith(String name);
}
