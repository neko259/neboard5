package me.neboard.newneboard.repository;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>, QueryByExampleExecutor<Post> {
    int countByPubTimeGreaterThan(Date before);
    int countByThread(PostThread thread);
    Page<Post> findByTitleContainingIgnoreCaseOrTextContainingIgnoreCase(Pageable pageable, String title, String text);
    List<Post> findByOpeningAndThreadBumpTimeGreaterThanOrderByIdDesc(boolean opening, Date date);
    List<Post> findByOpeningAndThreadTagsInAndThreadBumpTimeGreaterThanOrderByIdDesc(boolean opening, List<Tag> tags, Date date);
    List<Post> findByPosterAddressAndIdGreaterThanEqual(String posterAddress, long id);

    Page<Post> findByPostAttachmentsAttachmentFilename(Pageable pageable, String attachmentFilename);
    Page<Post> findByThreadAndOpening(Pageable pageable, PostThread thread, boolean opening);
    List<Post> findByPostAttachmentsAttachmentFilename(String attachmentFilename);

    <T> List<T> findByThread(PostThread thread, Class<T> type);
    <T> List<T> findByThreadAndIdIn(PostThread thread, List<Long> id, Class<T> type);
}
