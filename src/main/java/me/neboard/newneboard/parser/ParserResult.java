package me.neboard.newneboard.parser;

import java.io.Serializable;
import java.util.List;

public class ParserResult implements Serializable {
    private static final long serialVersionUID = 1L;

    private String text;
    private List<Long> reflinks;
    private List<ExternalPanelNode> tags;
    private List<ParserMethod> methods;

    public String getText() {
        return text;
    }

    public List<Long> getReflinks() {
        return reflinks;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setReflinks(List<Long> reflinks) {
        this.reflinks = reflinks;
    }

    public List<ExternalPanelNode> getTags() {
        return tags;
    }

    public void setTags(List<ExternalPanelNode> tags) {
        this.tags = tags;
    }

    public List<ParserMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<ParserMethod> methods) {
        this.methods = methods;
    }
}
