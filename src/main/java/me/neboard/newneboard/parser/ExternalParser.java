package me.neboard.newneboard.parser;

import me.neboard.newneboard.domain.PersistentEntity;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.ExternalRequestException;
import me.neboard.newneboard.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ExternalParser implements TextParser {
    public static final String CACHE_PARSER_METHODS = "parserMethods";

    private static final String HTML_PANEL_BUTTON = "<span class=\"mark_btn\" data-tag-left=\"%s\" data-tag-right=\"%s\" data-needs-input=%b data-input-hint=\"%s\">%s</span>";

    private static final String PARAM_RAW_TEXT = "raw_text";
    private static final String PARAM_POST_MAPPING = "post_mapping";

    private static final String EXTERNAL_METHOD_PREPARSE = "preparse";
    private static final String EXTERNAL_METHOD_REFLINKS = "reflinks";
    private static final String EXTERNAL_METHOD_PARSE = "parse";
    private static final String EXTERNAL_METHOD_TAGS = "tags";
    private static final String EXTERNAL_METHOD_METHODS = "list";

    @Autowired
    private ExternalService externalService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private JsonService jsonService;

    /**
     * Get a list of nodes to show in form formatting panel. Not all tags are shown,
     * some require additinal parameters and are not intended to be invoked from the panel.
     */
    public List<ExternalPanelNode> getPanelNodes(String method) throws BusinessException {
        ParserResult response = externalService.requestGet(
                externalService.getParserServiceUrl() + "/" + EXTERNAL_METHOD_TAGS + "/" + method,
                Collections.EMPTY_MAP,
                ParserResult.class);

        return response.getTags();
    }

    /**
     * Perform preparsing that is intended to replace the raw text.
     * Changes custom markdown-like patterns to proper bbcode that can be parsed
     */
    @Override
    public String preparse(String method, String rawText) throws BusinessException {
        return runParserRequest(EXTERNAL_METHOD_PREPARSE, method, Collections.singletonMap(PARAM_RAW_TEXT, rawText)).getText();
    }

    /**
     * Parse raw text into text for rendering. Line breaks are changed to custom intervals.
     */
    @Override
    public ParserResult parse(String method, String rawText, ParserLinkResolver resolver) throws BusinessException {
        Map<String, Object> params = new HashMap<>();
        params.put(PARAM_RAW_TEXT, rawText);

        ParserResult result = runParserRequest(EXTERNAL_METHOD_REFLINKS, method, params);

        Map<Long, PersistentEntity> mappings = result.getReflinks().stream()
                .map(resolver::modelById)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(PersistentEntity::getId, o -> o));
        params.put(PARAM_POST_MAPPING, jsonService.toJson(mappings));

        result.setText(runParserRequest(EXTERNAL_METHOD_PARSE, method, params).getText());
        return result;
    }

    private String getPanelButton(PanelNode node) {
        return String.format(HTML_PANEL_BUTTON, node.getTagLeft(), node.getTagRight(), node.needsInput(), node.getInputHint(), node.getPreview());
    }

    @Override
    public String getPanelText(String method) throws BusinessException {
        try {
            return getPanelNodes(method).stream()
                    .map(this::getPanelButton)
                    .collect(Collectors.joining());
        } catch (ExternalRequestException re) {
            return null;
        }
    }

    private ParserResult runParserRequest(String requestMethod, String parseMethod, Map<String, Object> params) throws BusinessException {
        try {
            return externalService.requestPost(
                    String.format("%s/%s/%s", externalService.getParserServiceUrl(), parseMethod, requestMethod),
                    params,
                    ParserResult.class);
        } catch (BusinessException e) {
            throw new BusinessException("Error in parser service: " + e.getMessage());
        }
    }

    @Override
    public List<ParserMethod> getMethods() throws BusinessException {
        return cacheService.get(CACHE_PARSER_METHODS, "", () ->
                externalService.requestGet(
                        externalService.getParserServiceUrl() + "/" + EXTERNAL_METHOD_METHODS, Collections.emptyMap(),
                        ParserResult.class),
                ParserResult.class).getMethods();
    }

}
