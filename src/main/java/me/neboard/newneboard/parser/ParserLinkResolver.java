package me.neboard.newneboard.parser;

import me.neboard.newneboard.domain.PersistentEntity;

public interface ParserLinkResolver {
    PersistentEntity modelById(Long id);
}
