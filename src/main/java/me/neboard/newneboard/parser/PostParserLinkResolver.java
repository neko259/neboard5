package me.neboard.newneboard.parser;

import me.neboard.newneboard.abstracts.PostMapping;
import me.neboard.newneboard.domain.PersistentEntity;
import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.service.PostService;

public class PostParserLinkResolver implements ParserLinkResolver {
    private PostService postService;

    public PostParserLinkResolver(PostService postService) {
        this.postService = postService;
    }

    @Override
    public PersistentEntity modelById(Long id) {
        Post post = postService.findById(id);
        return post == null ? null : new PostMapping(post.getId(),
                postService.getUrl(post), post.isOpening());
    }
}
