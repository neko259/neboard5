package me.neboard.newneboard.parser;

import java.io.Serializable;

public class ParserMethod implements Serializable {
    private String name;
    private int lineBreaks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLineBreaks() {
        return lineBreaks;
    }

    public void setLineBreaks(int lineBreaks) {
        this.lineBreaks = lineBreaks;
    }
}
