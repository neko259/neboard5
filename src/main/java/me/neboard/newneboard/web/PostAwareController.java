package me.neboard.newneboard.web;

public interface PostAwareController {
    String ATTR_POST = "post";
    String ATTR_THREAD = "thread";
    String ATTR_PREVIEW = "preview";

    String TEMPLATE_POST = "post";

    String ATTR_API_BLOCKS = "apiBlocks";
}
