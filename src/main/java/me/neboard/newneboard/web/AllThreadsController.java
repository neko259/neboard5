package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.form.ThreadForm;
import me.neboard.newneboard.rest.object.BlockModel;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.TagService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AllThreadsController implements PostAwareController, FormAware, PageAware {
    public static final String URL_ALL_THREADS = "/";
    public static final String URL_TAG = "/tag/";

    private static final String URL_TAG_THREADS = URL_TAG + "{tagName}";

    private static final String ATTR_TAG = "tag";
    private static final String ATTR_FAVORITE = "favorite";
    private static final String ATTR_HIDDEN = "hidden";
    private static final String ATTR_ACTIVE_THREADS_URL = "activeThreadsApiUrl";

    private static final String MAPPING_ALL_THREADS = "all_threads";

    @Autowired
    private PostService postService;

    @Autowired
    private TagService tagService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private UtilService utilService;

    @GetMapping(URL_ALL_THREADS)
    public String getAllThreads(@ModelAttribute(ATTR_FORM) ThreadForm form,
                                @RequestParam Optional<Integer> page,
                                @RequestParam Optional<Integer> fromId,
                                Model model) {
        return renderThreads(model, null, page, fromId.orElse(null));
    }

    private String renderThreads(Model model, Tag tag, Optional<Integer> page,
                                 Integer fromId) {
        List<BlockModel> blocks = new ArrayList<>();

        String apiUrl = "/api/post/";
        if (tag != null) {
            apiUrl += "tag/" + tag.getName() + "/";
            BlockModel tagInfo = new BlockModel("/api/tags/" + tag.getName() + "/");
            tagInfo.setTemplate("tag-info");
            blocks.add(tagInfo);
        }
        if (page.isPresent()) {
            apiUrl += "?page=" + page.get();
        }

        String activeThreadsApiUrl = "/api/post/active-threads/";
        if (tag != null) {
            activeThreadsApiUrl += tag.getName();
        }

        BlockModel activeThreads = new BlockModel(activeThreadsApiUrl);
        activeThreads.setTemplate("active-threads");
        blocks.add(activeThreads);

        BlockModel threads = new BlockModel(apiUrl, true);
        threads.addOption("openButton");
        threads.addOption("threadCounters");
        blocks.add(threads);

        String formApiUrl = "/api/post/form/?title="
                + utilService.getMessage("form.new.thread") + "&action=/api/post/&tags=true";
        if (fromId != null) {
            formApiUrl += "&fromId=" + fromId;
        }
        BlockModel formBlock = new BlockModel(formApiUrl);
        formBlock.setTemplate("form");
        blocks.add(formBlock);

        model.addAttribute(ATTR_API_BLOCKS, blocks);

        return MAPPING_ALL_THREADS;
    }

    @GetMapping(URL_TAG_THREADS)
    public String getTagThreads(@PathVariable String tagName, @ModelAttribute(ATTR_FORM) ThreadForm form, @RequestParam Optional<Integer> page, Model model) throws NotFoundException {
        Tag tag = tagService.findByName(tagName);

        if (tag == null) {
            throw new NotFoundException("Cannot find tag");
        }
        
        model.addAttribute(ATTR_TAG, tag);
        model.addAttribute(ATTR_FAVORITE, settingsService.isTagFavorite(tag));
        model.addAttribute(ATTR_HIDDEN, settingsService.isTagHidden(tag));

        form.setTags(tagName);

        return renderThreads(model, tag, page, null);
    }

}
