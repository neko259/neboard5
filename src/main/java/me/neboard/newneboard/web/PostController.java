package me.neboard.newneboard.web;

import me.neboard.newneboard.rest.object.BlockModel;
import me.neboard.newneboard.user.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class PostController implements PostAwareController, PageAware {
    public static final String URL_SEARCH = "/search/";
    public static final String URL_FEED = "/feed/";

    private static final String ATTR_QUERY = "query";
    private static final String ATTR_FEED_CRITERIA = "criteria";

    private static final String MAPPING_FEED = "feed";
    private static final String MAPPING_SEARCH = "search";


    @Autowired
    private SettingsService settingsService;

    @GetMapping(URL_FEED)
    public String feed(@RequestParam Optional<Integer> page,
            @RequestParam Optional<String> tripcode,
            @RequestParam Optional<String> address,
            @RequestParam Optional<String> filename,
            Model model, HttpServletRequest request) {
        String queryString = request.getQueryString();
        if (queryString == null) {
            queryString = "";
        }

        List<BlockModel> blocks = new ArrayList<>();
        BlockModel criteria = new BlockModel("/api/post/feed-criteria/?" + queryString);
        blocks.add(criteria);

        BlockModel feed = new BlockModel("/api/post/feed/?" + queryString, true);
        feed.addOption("threadUrl");
        blocks.add(feed);

        model.addAttribute(ATTR_API_BLOCKS, blocks);

        return MAPPING_FEED;
    }

    @GetMapping(URL_SEARCH)
    public String search(@RequestParam Optional<Integer> page,
                       @RequestParam Optional<String> query,
                       Model model) {
        String queryText = query.orElse("");

        model.addAttribute(ATTR_QUERY, queryText);

        List<BlockModel> blocks = new ArrayList<>();

        BlockModel form = new BlockModel("/api/post/search-form/?query=" + queryText);
        blocks.add(form);

        BlockModel results = new BlockModel("/api/post/search/?query=" + queryText, true);
        results.addOption("threadUrl");
        blocks.add(results);

        model.addAttribute(ATTR_API_BLOCKS, blocks);

        return MAPPING_SEARCH;
    }
}
