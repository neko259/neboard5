package me.neboard.newneboard.web;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.rest.object.BlockModel;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import jakarta.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class ThreadController implements PostAwareController, FormAware {
    public static final String URL_PREFIX_THREAD = "/thread/";

    private static final String URL_THREAD = URL_PREFIX_THREAD + "{openingPostId}";
    private static final String URL_GALLERY = URL_PREFIX_THREAD + "{openingPostId}/gallery";

    private static final String ATTR_THREAD = "thread";
    private static final String ATTR_UPDATE_TIMEOUT = "updateTimeout";
    private static final String ATTR_FORM_TITLE = "formTitle";
    private static final String ATTR_ATTACHMENTS = "attachments";
    private static final String ATTR_TITLE = "title";

    private static final String MAPPING_THREAD = "thread";
    private static final String MAPPING_GALLERY = "thread_gallery";

    private static final String HTML_FORM_TITLE = "%s #%d<span class='reply-to-message' style='display: none;'> %s #<span id='reply-to-message-id'></span></span>";

    private static final String TITLE_REPLY_THREAD = "form.reply.thread";
    private static final String TITLE_REPLY_MESSAGE = "form.reply.message";

    @Autowired
    private PostService postService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private UtilService utilService;

    @Value("${thread.update.timeout}")
    private long updateTimeout;

    @GetMapping(URL_THREAD)
    public String getThread(@PathVariable Long openingPostId, @ModelAttribute(ATTR_FORM) PostForm form, Model model,
            HttpServletResponse response) throws NotFoundException, UnsupportedEncodingException {
        String mapping;
        Post threadPost = postService.findById(openingPostId);
        if (threadPost == null) {
            throw new NotFoundException("Thread not found");
        } else if (!threadPost.isOpening()) {
            mapping = UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(threadPost);
        } else {
            PostThread thread = threadPost.getThread();
            model.addAttribute(ATTR_THREAD, thread);
            model.addAttribute(ATTR_UPDATE_TIMEOUT, updateTimeout);
            model.addAttribute(ATTR_TITLE, getTitle(thread));

            List<BlockModel> blocks = new ArrayList<>();
            BlockModel postsBlock = new BlockModel("/api/post/" + thread.getOpeningPost().getId() + "/thread");
            postsBlock.addOption("replyButton");
            postsBlock.addAttribute("data-thread-id", thread.getOpeningPost().getId());
            blocks.add(postsBlock);

            if (thread.canPost()) {
                BlockModel formBlock = new BlockModel("/api/post/form/?title="
                        + URLEncoder.encode(String.format(HTML_FORM_TITLE,
                        utilService.getMessage(TITLE_REPLY_THREAD),
                        thread.getOpeningPost().getId(),
                        utilService.getMessage(TITLE_REPLY_MESSAGE)), "utf-8")
                        + "&action=/api/post/" + openingPostId + "/");
                formBlock.setTemplate("form");
                blocks.add(formBlock);
            }

            model.addAttribute(ATTR_API_BLOCKS, blocks);

            mapping = MAPPING_THREAD;
        }

        utilService.noCacheResponse(response);

        return mapping;
    }

    @GetMapping(URL_GALLERY)
    public String getGallery(@PathVariable Long openingPostId, @ModelAttribute(ATTR_FORM) PostForm form, Model model,
                            HttpServletResponse response) throws NotFoundException {
        String mapping;
        Post threadPost = postService.findById(openingPostId);
        if (threadPost == null) {
            throw new NotFoundException("Thread not found");
        } else if (!threadPost.isOpening()) {
            mapping = UrlBasedViewResolver.REDIRECT_URL_PREFIX + postService.getUrl(threadPost);
        } else {
            PostThread thread = threadPost.getThread();

            BlockModel attachments = new BlockModel("/api/post/" + thread.getOpeningPost().getId() + "/gallery");
            attachments.addAttribute("data-thread-id", thread.getOpeningPost().getId());

            model.addAttribute(ATTR_API_BLOCKS, Collections.singletonList(attachments));

            mapping = MAPPING_THREAD;
        }

        return mapping;
    }

    private String getTitle(PostThread thread) {
        return postService.getShortTitle(thread.getOpeningPost());
    }

}
