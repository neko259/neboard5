package me.neboard.newneboard.web;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.form.SettingsForm;
import me.neboard.newneboard.form.LoginForm;
import me.neboard.newneboard.form.validation.LoginFormValidator;
import me.neboard.newneboard.service.ParserService;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import jakarta.validation.Valid;
import java.util.*;

@Controller
public class SettingsController {
    private static final String URL_SETTINGS = "/settings";
    private static final String URL_LOGIN = "/login";

    private static final String ATTR_THEMES = "themes";
    private static final String ATTR_PARSE_METHODS = "parseMethods";
    private static final String ATTR_USER = "user";

    @Value("#{'${themes}'.split(',')}")
    private List<String> themes;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private UserService userService;

    @Autowired
    private LoginFormValidator validator;

    @Autowired
    private ParserService parserService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        if (binder.getTarget() instanceof LoginForm) {
            binder.setValidator(validator);
        }
    }

    @GetMapping(URL_SETTINGS)
    public String getSettings(@ModelAttribute("form") SettingsForm form, Model model) throws BusinessException {
        model.addAttribute(ATTR_THEMES, settingsService.getThemes());

        model.addAttribute(ATTR_PARSE_METHODS, parserService.getMethods());
        model.addAttribute(ATTR_USER, userService.getUser().getUuid());

        form.setTheme(settingsService.getTheme());
        form.setParseMethod(settingsService.getParseMethod());

        return "settings";
    }

    @GetMapping(URL_LOGIN)
    public String getLogin(@ModelAttribute("form") LoginForm form,
                           @RequestParam String uuid, Model model) {
        form.setUser(uuid);

        return "login";
    }

    @PostMapping(URL_LOGIN)
    public String login(@Valid @ModelAttribute("form") LoginForm form, BindingResult bindingResult, Model model) throws BusinessException {
        if (bindingResult.hasErrors()) {
            return getLogin(form, form.getUser(), model);
        } else {
            userService.login(form.getUser());
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + URL_SETTINGS;
        }
    }

    @PostMapping(URL_SETTINGS)
    public String saveSettings(@Valid @ModelAttribute("form") SettingsForm form, BindingResult bindingResult, Model model) throws BusinessException {
        if (bindingResult.hasErrors()) {
            return getSettings(form, model);
        } else {
            settingsService.saveTheme(form.getTheme());
            settingsService.saveParseMethod(form.getParseMethod());
            return UrlBasedViewResolver.REDIRECT_URL_PREFIX + URL_SETTINGS;
        }
    }
}
