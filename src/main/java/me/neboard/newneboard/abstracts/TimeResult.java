package me.neboard.newneboard.abstracts;

public class TimeResult {
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
