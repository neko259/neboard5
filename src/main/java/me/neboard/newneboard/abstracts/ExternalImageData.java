package me.neboard.newneboard.abstracts;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class ExternalImageData implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<String> names;

    public ExternalImageData(String[] names) {
        this.names = Arrays.asList(names);
    }

    public ExternalImageData() {
    }

    public String getName() {
        if (names.isEmpty()) {
            return null;
        } else {
            return names.iterator().next();
        }
    }

    public boolean hasData() {
        return !names.isEmpty();
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
