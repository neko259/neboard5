package me.neboard.newneboard.abstracts;

import me.neboard.newneboard.domain.PersistentEntity;

import java.io.Serializable;

public class PostMapping implements Serializable, PersistentEntity {
    private static final long serialVersionUID = 1L;

    private long id;
    private String url;
    private boolean opening;

    public PostMapping(long id, String url, boolean opening) {
        this.id = id;
        this.url = url;
        this.opening = opening;
    }

    public String getUrl() {
        return url;
    }

    public boolean isOpening() {
        return opening;
    }

    public Long getId() {
        return id;
    }
}
