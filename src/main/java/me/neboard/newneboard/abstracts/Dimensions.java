package me.neboard.newneboard.abstracts;

import java.io.Serializable;

public class Dimensions implements Serializable {
    private static final long serialVersionUID = 1L;

    private final long width;
    private final long height;

    public Dimensions(long width, long height) {
        this.width = width;
        this.height = height;
    }

    public long getWidth() {
        return width;
    }

    public long getHeight() {
        return height;
    }
}
