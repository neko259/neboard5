package me.neboard.newneboard.abstracts;

public class ExternalCacheMeta {
    private String cacheName;
    private String cacheUpdateUrl;
    private String cacheUpdateTime;
    boolean timeSupported = false;

    public ExternalCacheMeta(String cacheName, String cacheUpdateUrl) {
        this.cacheName = cacheName;
        this.cacheUpdateUrl = cacheUpdateUrl;
        timeSupported = true;
    }

    public ExternalCacheMeta(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheName() {
        return cacheName;
    }

    public String getCacheUpdateUrl() {
        return cacheUpdateUrl;
    }

    public String getCacheUpdateTime() {
        return cacheUpdateTime;
    }

    public void setCacheUpdateTime(String cacheUpdateTime) {
        this.cacheUpdateTime = cacheUpdateTime;
    }

    public boolean isTimeSupported() {
        return timeSupported;
    }
}
