package me.neboard.newneboard.rest;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.rest.object.ModelsPage;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@RestController
@RequestMapping(TagRestController.URL)
public class TagRestController implements ApiRestController {
	public static final String URL = ApiRestController.URL + "tags";

	private static final String URL_TAGS = "/";

	private static final String URL_TAG_FAV_ADD = "/{name}/add-favorite";
	private static final String URL_TAG_FAV_REMOVE = "/{name}/remove-favorite";
	private static final String URL_TAG_HIDDEN_ADD = "/{name}/add-hidden";
	private static final String URL_TAG_HIDDEN_REMOVE = "/{name}/remove-hidden";
	private static final String URL_TAG = "/{name}/";

	@Autowired
	private TagService tagService;

	@Autowired
	private SettingsService settingsService;

	// FIXME Use response entity
	@GetMapping(URL_TAGS)
	public Set<String> filteredTags(@RequestParam String term) {
		SortedSet<String> tagNames = new TreeSet<>();

		tagService.findStartingWith(term)
				.forEach(tag -> tagNames.add(tag.getName()));

		return tagNames;
	}

	@GetMapping(URL_TAG)
	public ResponseEntity<ModelsPage> getTag(@PathVariable String name) throws BusinessException {
		Tag tag = tagService.findByName(name);
		if (tag == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(new ModelsPage(tagService.getModel(tag)));
		}
	}

	@PostMapping(URL_TAG_FAV_ADD)
	public ResponseEntity<Void> addFavTag(@PathVariable String name) {
		Tag tag = tagService.findByName(name);
		settingsService.addFavoriteTag(tag);
		return ResponseEntity.ok().build();
	}

	@PostMapping(URL_TAG_FAV_REMOVE)
	public ResponseEntity<Void> removeFavTag(@PathVariable String name) {
		Tag tag = tagService.findByName(name);
		settingsService.removeFavoriteTag(tag);
		return ResponseEntity.ok().build();
	}

	@PostMapping(URL_TAG_HIDDEN_ADD)
	public ResponseEntity<Void> hideTag(@PathVariable String name) {
		Tag tag = tagService.findByName(name);
		settingsService.addHiddenTag(tag);
		return ResponseEntity.ok().build();
	}

	@PostMapping(URL_TAG_HIDDEN_REMOVE)
	public ResponseEntity<Void> unhideTag(@PathVariable String name) {
		Tag tag = tagService.findByName(name);
		settingsService.removeHiddenTag(tag);
		return ResponseEntity.ok().build();
	}

}
