package me.neboard.newneboard.rest;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.ExternalService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DependentServicesInfoContributor implements InfoContributor {
    private static final String URL_HEALTH = "/health";

    @Autowired
    private ExternalService externalService;

    @Override
    public void contribute(Info.Builder builder) {
        long totalMemory = Runtime.getRuntime().totalMemory();
        long usedMemory = totalMemory - Runtime.getRuntime().freeMemory();

        builder.withDetail("totalMemory", FileUtils.byteCountToDisplaySize(totalMemory));
        builder.withDetail("usedMemory", FileUtils.byteCountToDisplaySize(usedMemory));
        builder.withDetail("maxMemory", FileUtils.byteCountToDisplaySize(Runtime.getRuntime().maxMemory()));

        Map<String, Object> services = new HashMap<>();
        services.put("parser", isServiceHealthy(externalService.getParserServiceUrl()));
        services.put("files", isServiceHealthy(externalService.getFileServiceUrl()));
        builder.withDetail("services", services);
    }

    private boolean isServiceHealthy(String serviceUrl) {
        try {
            return externalService.requestGet(serviceUrl + URL_HEALTH, null, Object.class) == null;
        } catch (BusinessException e) {
            return false;
        }
    }

}
