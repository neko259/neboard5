package me.neboard.newneboard.rest;

import me.neboard.newneboard.rest.object.StatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public abstract class FormProcessingRestController {
    @Autowired
    private ApplicationContext appContext;

    private String resolveErrorMessage(ObjectError fieldError) {
        return appContext.getMessage(fieldError.getCode(), fieldError.getArguments(), LocaleContextHolder.getLocale());
    }

    protected void saveErrorMessages(BindingResult result, StatusResponse response) {
        result.getAllErrors().forEach(fieldError -> response.addError(resolveErrorMessage(fieldError)));
    }
}
