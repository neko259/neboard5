package me.neboard.newneboard.rest;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.service.BanService;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AdminRestController.URL)
public class AdminRestController implements ApiRestController {
    public static final String URL = ApiRestController.URL + "admin";

    private static final String URL_BAN = "/ban";
    private static final String URL_CLEANUP = "/cleanup/{text}";

    @Autowired
    private PostService postService;

    @Autowired
    private BanService banService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private ThreadService threadService;

    @PostMapping(URL_BAN)
    public ResponseEntity<Void> banUser(@RequestParam String address, @RequestParam long postId)
            throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        banService.banUser(address, "Post #" + post.getId());

        return ResponseEntity.noContent().build();
    }

    @PostMapping(URL_CLEANUP)
    public ResponseEntity<Void> cleanup(@PathVariable String text)
            throws PermissionException {
        settingsService.validatePermission();

        List<Post> posts = postService.search(text);
//        postService.purgePosts(posts, true);

        return ResponseEntity.noContent().build();
    }

}
