package me.neboard.newneboard.rest;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.parser.ParserLinkResolver;
import me.neboard.newneboard.parser.PostParserLinkResolver;
import me.neboard.newneboard.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(ApiRestController.URL)
public class NeboardRestController implements ApiRestController {
    private static final String URL_PREVIEW = "/preview";
    private static final String URL_CONSISTENCY = "/consistency";
    private static final String URL_EXPORT = "/export";

    private static final String PARAM_RAW_TEXT = "raw_text";


    @Autowired
    private ParserService parserService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private PostService postService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private ExternalService externalService;

    private ParserLinkResolver resolver;

    @PostConstruct
    private void initParserResolver() {
        resolver = new PostParserLinkResolver(postService);
    }

    @PostMapping(URL_PREVIEW)
    public ResponseEntity<String> textPreview(@RequestParam(PARAM_RAW_TEXT) String text) throws BusinessException {
        return ResponseEntity.ok(parserService.parse(parserService.preparse(text), resolver).getText());
    }

    @GetMapping(value = URL_CONSISTENCY, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> consistency() {
        Map<String, Object> status = new HashMap<>();

        status.put("Missing Files", attachmentService.getMissingFiles());
        status.put("Attachment Count", attachmentService.getCount());
        status.put("Post Count", postService.getCount());

        return status;
    }

    @GetMapping(value = URL_EXPORT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> export() {
        Map<String, Object> status = new HashMap<>();

        status.put("attachments", attachmentService.getFileUrlList());

        return status;
    }


}
