package me.neboard.newneboard.rest.object;

public class PostRef {
    public static final String DELIMITER = ":";

    private long id;
    private String url;
    private boolean opening;

    public PostRef() {
    }

    public PostRef(long id, String url, boolean opening) {
        this.id = id;
        this.url = url;
        this.opening = opening;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isOpening() {
        return opening;
    }

    public void setOpening(boolean opening) {
        this.opening = opening;
    }

    @Override
    public String toString() {
        return getId() + DELIMITER + getUrl() + DELIMITER + opening;
    }

}
