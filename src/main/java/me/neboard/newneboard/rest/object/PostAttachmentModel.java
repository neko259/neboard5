package me.neboard.newneboard.rest.object;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostAttachmentModel implements Serializable {
    private Integer order;
    private AttachmentModel attachment;

    public PostAttachmentModel(AttachmentModel attachmentModel) {
        this.attachment = attachmentModel;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getOrder() {
        return order;
    }

    public AttachmentModel getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentModel attachment) {
        this.attachment = attachment;
    }
}
