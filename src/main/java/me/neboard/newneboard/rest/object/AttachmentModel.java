package me.neboard.newneboard.rest.object;

import java.util.Map;

public class AttachmentModel {
    private final String type = "attachment";

    private String viewerType;
    private String filename;
    private String url;
    private Map<String, String> data;

    public AttachmentModel(String filename, String url, String viewerType, Map<String, String> data) {
        this.viewerType = viewerType;
        this.data = data;
        this.filename = filename;
        this.url = url;
    }

    public String getViewerType() {
        return viewerType;
    }

    public String getType() {
        return type;
    }

    public String getFilename() {
        return filename;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, String> getData() {
        return data;
    }
}
