package me.neboard.newneboard.rest.object;

public class ThreadRef {
    private long id;
    private String url;
    private String title;

    public ThreadRef(long id, String url, String title) {
        this.id = id;
        this.url = url;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
