package me.neboard.newneboard.rest.object;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PostModel {
	private Long thread;
	private String threadTitle;
	private String text;
	private String tripcode;
	private String title;
	private Long id;
	private List<PostRef> replies;
	private Date pubTime;
	private List<String> tags;
	private String uuid;
	private String posterAddress;
	private final String type = "post";
	private Integer postCount;
	private Integer attachmentCount;
	private String threadStatus;

	// TODO Remove this when sources bot learns to send an attachment
	private List<String> urls;

	private List<PostAttachmentModel> attachments;
	private String url;
	private boolean opening;

	public Long getThread() {
		return thread;
	}

	public String getText() {
		return text;
	}

	public String getTripcode() {
		return tripcode;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getUrls() {
		return urls;
	}

	public String getUrl() {
		return url;
	}

	public boolean isOpening() {
		return opening;
	}

	public List<PostAttachmentModel> getAttachments() {
		return attachments;
	}

	public void setThread(Long thread) {
		this.thread = thread;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTripcode(String tripcode) {
		this.tripcode = tripcode;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setOpening(boolean opening) {
		this.opening = opening;
	}

	public void setAttachments(List<PostAttachmentModel> attachments) {
		this.attachments = attachments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<PostRef> getReplies() {
		return replies;
	}

	public void setReplies(List<PostRef> replies) {
		this.replies = replies;
	}

	public Date getPubTime() {
		return pubTime;
	}

	public void setPubTime(Date pubTime) {
		this.pubTime = pubTime;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getPosterAddress() {
		return posterAddress;
	}

	public void setPosterAddress(String posterAddress) {
		this.posterAddress = posterAddress;
	}

	public String getType() {
		return type;
	}

	public Integer getPostCount() {
		return postCount;
	}

	public void setPostCount(Integer postCount) {
		this.postCount = postCount;
	}

	public Integer getAttachmentCount() {
		return attachmentCount;
	}

	public void setAttachmentCount(Integer attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	public String getThreadStatus() {
		return threadStatus;
	}

	public void setThreadStatus(String threadStatus) {
		this.threadStatus = threadStatus;
	}

	public String getThreadTitle() {
		return threadTitle;
	}

	public void setThreadTitle(String threadTitle) {
		this.threadTitle = threadTitle;
	}
}
