package me.neboard.newneboard.rest.object;

import java.util.List;

public class GalleryModel {
    private final String type = "gallery";
    private long id;

    private List<AttachmentModel> attachments;

    public GalleryModel(long id, List<AttachmentModel> attachments) {
        this.attachments = attachments;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public List<AttachmentModel> getAttachments() {
        return attachments;
    }

    public long getId() {
        return id;
    }
}
