package me.neboard.newneboard.rest.object;

import java.util.Collections;
import java.util.List;

public class ModelsPage {
    private List<Object> models;
    private int pageNumber = 0;
    private int pages = 1;
    private String title;

    private String apiUrl;
    private String webUrl;


    public ModelsPage(List<Object> models, int pageNumber,
                      int pages, String apiUrl, String webUrl) {
        this.models = models;
        this.pageNumber = pageNumber;
        this.pages = pages;
        this.apiUrl = apiUrl;
        this.webUrl = webUrl;
    }

    public ModelsPage(List<Object> models) {
        this.models = models;
    }

    public ModelsPage(Object model) {
        this.models = Collections.singletonList(model);
    }

    public List<Object> getModels() {
        return models;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPages() {
        return pages;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
