package me.neboard.newneboard.rest.object;

public class CriteriaModel {
    private final String type = "feed-criteria";

    private String key;
    private String value;

    public CriteriaModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
