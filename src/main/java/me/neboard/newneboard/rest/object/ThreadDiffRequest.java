package me.neboard.newneboard.rest.object;

import java.util.List;

public class ThreadDiffRequest {
    private Long threadId;
    private List<String> uuids;

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public List<String> getUuids() {
        return uuids;
    }

    public void setUuids(List<String> uuids) {
        this.uuids = uuids;
    }
}
