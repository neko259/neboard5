package me.neboard.newneboard.rest.object;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusResponse {
    public Map<String, Object> data = new HashMap<>();
    private List<String> errors = new ArrayList<>();
    private String url;

    public StatusResponse() {

    }

    public StatusResponse(String url) {
        this.url = url;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        errors.add(error);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void addData(String key, Object value) {
        data.put(key, value);
    }
}
