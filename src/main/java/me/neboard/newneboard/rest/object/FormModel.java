package me.neboard.newneboard.rest.object;

public class FormModel {
    private String formTitle;
    private String action;
    private boolean edit;
    private boolean tagsEnabled;

    private String title = "";
    private String tags = "";
    private String text = "";

    public FormModel(String formTitle, String action, boolean edit, boolean tagsEnabled) {
        this.formTitle = formTitle;
        this.action = action;
        this.edit = edit;
        this.tagsEnabled = tagsEnabled;
    }

    public String getFormTitle() {
        return formTitle;
    }

    public String getAction() {
        return action;
    }

    public boolean isEdit() {
        return edit;
    }

    public boolean isTagsEnabled() {
        return tagsEnabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
