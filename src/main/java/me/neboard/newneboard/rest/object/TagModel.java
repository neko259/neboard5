package me.neboard.newneboard.rest.object;

public class TagModel {
    private final String type = "tag";
    private String name;
    private boolean favorite;
    private boolean hidden;

    public TagModel(String name) {
        this.name = name;
    }

    public TagModel(String name, boolean favorite, boolean hidden) {
        this.name = name;
        this.favorite = favorite;
        this.hidden = hidden;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public boolean isHidden() {
        return hidden;
    }
}
