package me.neboard.newneboard.rest.object;

import java.util.List;

public class ThreadModel {
    private final String type = "thread";

    private long id; // opening post id, not thread id
    private String title;
    private String status;
    private List<PostModel> posts;
    private int postLimit;

    public ThreadModel(long id, String title, String status,
                       List<PostModel> posts, int postLimit) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.posts = posts;
        this.postLimit = postLimit;
    }

    public String getType() {
        return type;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getStatus() {
        return status;
    }

    public List<PostModel> getPosts() {
        return posts;
    }

    public int getPostLimit() {
        return postLimit;
    }
}
