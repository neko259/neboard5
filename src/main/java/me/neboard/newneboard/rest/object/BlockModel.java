package me.neboard.newneboard.rest.object;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockModel {
    private String apiUrl;
    private List<String> options = new ArrayList<>();
    private Map<String, Object> attributes = new HashMap<>();
    private String template;
    private boolean pageable;

    public BlockModel(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public BlockModel(String apiUrl, boolean pageable) {
        this.apiUrl = apiUrl;
        this.pageable = pageable;
    }

    public List<String> getOptions() {
        return options;
    }

    public void addOption(String option) {
        options.add(option);
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public boolean isPageable() {
        return pageable;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void addAttribute(String key, Object value) {
        attributes.put(key, value);
    }

    @Override
    public String toString() {
        String template = "";
        if (getTemplate() != null) {
            template = " data-template='" + getTemplate() + "' ";
        }
        StringBuilder attributeSts = new StringBuilder();
        for (String key : getAttributes().keySet()) {
            attributeSts.append(" ")
                    .append(key)
                    .append("='")
                    .append(getAttributes().get(key))
                    .append("' ");
        }
        attributeSts.append(template);

        // FIXME This is temporary until front-end learns how to render a block
        return String.format("<div data-async='%s' data-options='%s' %s data-pageable='%s'></div>",
                getApiUrl(), String.join(" ", getOptions()), attributeSts, isPageable());

    }
}
