package me.neboard.newneboard.rest.object;

import java.util.List;

public class ThreadPreviewModel {
    private final String type = "thread-preview";
    private PostModel openingPost;
    private List<PostModel> lastPosts;
    private int skippedPosts;

    public ThreadPreviewModel(PostModel openingPost, List<PostModel> lastPosts, int skippedPosts) {
        this.openingPost = openingPost;
        this.lastPosts = lastPosts;
        this.skippedPosts = skippedPosts;
    }

    public String getType() {
        return type;
    }

    public PostModel getOpeningPost() {
        return openingPost;
    }

    public List<PostModel> getLastPosts() {
        return lastPosts;
    }

    public int getSkippedPosts() {
        return skippedPosts;
    }
}
