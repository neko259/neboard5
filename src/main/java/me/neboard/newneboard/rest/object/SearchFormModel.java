package me.neboard.newneboard.rest.object;

public class SearchFormModel {
    private final String type = "search-form";

    private String query;
    private boolean admin;

    public SearchFormModel(String query, boolean admin) {
        this.query = query;
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public String getQuery() {
        return query;
    }

    public String getType() {
        return type;
    }
}
