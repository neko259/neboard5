package me.neboard.newneboard.rest;

import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.domain.projection.PostIdAndUuid;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.ThreadForm;
import me.neboard.newneboard.form.validation.PostFormValidator;
import me.neboard.newneboard.form.validation.ThreadFormValidator;
import me.neboard.newneboard.rest.object.*;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.web.AllThreadsController;
import me.neboard.newneboard.web.PostAwareController;
import me.neboard.newneboard.web.PostController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(PostRestController.URL)
public class PostRestController extends FormProcessingRestController implements PostAwareController, ApiRestController {
    public static final String URL = ApiRestController.URL + "post";

    public static final String URL_THREAD_DIFF = "/diff/";

    private static final String URL_THREADS = "/";
    private static final String URL_TAG_THREADS = "/tag/{tagName}/";
    private static final String URL_ADD_POST = "/{openingPostId}";
    private static final String URL_GET_POST = "/{postId}";
    private static final String URL_GET_THREAD = "/{postId}/thread";
    private static final String URL_GET_GALLERY = "/{postId}/gallery";
    private static final String URL_GET_REPLIES = "/{postId}/replies";
    private static final String URL_DELETE_POST = "/{postId}/delete";
    private static final String URL_PURGE = "/purge/";
    private static final String URL_SEARCH = "/search/";
    private static final String URL_SEARCH_FORM = "/search-form/";
    private static final String URL_FEED = "/feed/";
    private static final String URL_FEED_CRITERIA = "/feed-criteria/";
    private static final String URL_ACTIVE_THREADS = "/active-threads/";
    private static final String URL_FORM = "/form/";
    private static final String URL_TAG_ACTIVE_THREADS = "/active-threads/{tagName}";

    private static final String URL_SERVICE_ADD_POST = "/service/add-post";

    private static final String HEADER_API_KEY = "X-API-SECRET";

    private static final String ATTR_UPDATED_POSTS = "updated";
    private static final String ATTR_DELETED_POSTS = "deleted";
    private static final String ATTR_THREAD_POST_LIMIT = "postLimit";
    private static final String ATTR_THREAD_STATUS = "threadStatus";

    private static final String CRITERIA_TRIPCODE = "criteria.tripcode";
    private static final String CRITERIA_ADDRESS = "criteria.address";
    private static final String CRITERIA_ATTACHMENT = "criteria.attachment";

    @Value("#{'${api.secrets}'.split(',')}")
    private List<String> apiSecrets;

    @Value("${api.posts.per.page}")
    private int postsPerPage;

    @Value("${threads.per.page}")
    private int threadsPerPage;

    @Autowired
    @Qualifier("postFormValidator")
    private PostFormValidator postValidator;

    @Autowired
    private ThreadFormValidator threadValidator;

    @Autowired
    private PostService postService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private ThreadPostService threadPostService;

    @Autowired
    private TagService tagService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private ViewService viewService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private TripcodeService tripcodeService;

    @Autowired
    private FormAttachmentService formAttachmentService;

    @Autowired
    private BanService banService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();
        if (target != null) {
            Validator validator = null;
            if (target instanceof ThreadForm) {
                validator = threadValidator;
            } else if (target instanceof PostForm) {
                validator = postValidator;
            }

            if (validator != null) {
                binder.setValidator(validator);
            }
        }
    }

    // Replace with AMQP-based messages
    @Deprecated
    @PostMapping(value = URL_SERVICE_ADD_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> serviceAddPost(@RequestBody PostModel postModel, HttpServletRequest request) throws BusinessException {
        String apiKey = request.getHeader(HEADER_API_KEY);
        
        boolean valid = apiSecrets.contains(apiKey);

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            entity = ResponseEntity.badRequest().body(response);
            response.addError("Invalid API key");
        } else {
            entity = ResponseEntity.ok(response);
        }

        PostThread thread = postService.findById(postModel.getThread()).getThread();

        List<FormAttachment> urls = new ArrayList<>();
        if (postModel.getUrls() != null) {
            urls = postModel.getUrls().stream()
                    .map(formAttachmentService::getAttachment)
                    .collect(Collectors.toList());
        }

        List<Attachment> attachments = formAttachmentService.createFromFormData(urls);

        try {
            PostCreateDetails details = new PostCreateDetails(postModel.getTitle(),
                    postModel.getText(), attachments, thread, null,
                    tripcodeService.encode(postModel.getTripcode()));
            postService.create(details);

            thread = threadService.findById(thread.getId());
            response.addData(ATTR_THREAD_STATUS, thread.getStatus());
        } catch (BusinessException e) {
            response.addError(e.getMessage());
            entity = ResponseEntity.badRequest().body(response);
        }

        return entity;
    }

    @PostMapping(value = URL_ADD_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> addPost(@PathVariable Long openingPostId, @Valid PostForm form,
            BindingResult result, HttpServletRequest request) throws BusinessException {
        boolean valid = !result.hasErrors();

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            saveErrorMessages(result, response);
            entity = ResponseEntity.badRequest().body(response);
        } else {
            entity = ResponseEntity.ok(response);
            PostThread thread = postService.findById(openingPostId).getThread();

            try {
                PostCreateDetails details = postService.getCreateDetails(form,
                        thread, utilService.getIpFromRequest(request));
                postService.create(details);
            } catch (BusinessException e) {
                response.addError(e.getMessage());
                entity = ResponseEntity.badRequest().body(response);
            }
        }

        return entity;
    }

    @PostMapping(value = URL_THREAD_DIFF, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getThreadDiff(@RequestBody ThreadDiffRequest request) throws BusinessException {
        PostThread thread = postService.findById(request.getThreadId()).getThread();

        List<String> deletedPostIds = new ArrayList<>(request.getUuids());
        List<PostIdAndUuid> posts = postService.findByThread(thread, PostIdAndUuid.class);
        List<Long> updatedPostIds = new ArrayList<>();
        for (PostIdAndUuid post : posts) {
            String uuid = post.getUuid();
            if (deletedPostIds.contains(uuid)) {
                deletedPostIds.remove(uuid);
            } else {
                updatedPostIds.add(post.getId());
            }
        }

        List<Post> updatedPosts = postService.findByThreadAndIdIn(thread, updatedPostIds, Post.class);

        List<PostModel> updatedPostModels = new ArrayList<>();
        for (Post post : updatedPosts) {
            updatedPostModels.add(postService.getModel(post));
        }

        Map<String, Object> result = new HashMap<>();
        result.put(ATTR_UPDATED_POSTS, updatedPostModels);
        result.put(ATTR_DELETED_POSTS, deletedPostIds);
        result.put(ATTR_THREAD_POST_LIMIT, thread.getPostLimit());
        result.put(ATTR_THREAD_STATUS, thread.getStatus());

        return result;
    }

    @GetMapping(value = URL_GET_POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostModel> getPost(@PathVariable Long postId, HttpServletRequest request) throws BusinessException {
        ResponseEntity<PostModel> entity;
        Post post = postService.findById(postId);
        if (post != null) {
            entity = ResponseEntity.ok(postService.getModel(post));
        } else {
            entity = ResponseEntity.notFound().build();
        }

        return entity;
    }

    @GetMapping(value = URL_GET_THREAD, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModelsPage> getThread(@PathVariable Long postId, HttpServletRequest request) throws BusinessException {
        ResponseEntity<ModelsPage> entity;
        Post openingPost = postService.findById(postId);
        if (openingPost != null && openingPost.isOpening()) {
            PostThread thread = openingPost.getThread();
            List<Post> posts = thread.getPosts();
            List<PostModel> postModels = posts.stream()
                    .map(post -> postService.getModel(post, openingPost))
                    .collect(Collectors.toList());

            String shortTitle = postService.getShortTitle(openingPost);
            ThreadModel threadModel = new ThreadModel(openingPost.getId(),
                    shortTitle, thread.getStatus().toString(),
                    postModels, thread.getPostLimit());
            ModelsPage modelsPage = new ModelsPage(threadModel);
            modelsPage.setTitle(shortTitle);
            entity = ResponseEntity.ok(modelsPage);
        } else {
            entity = ResponseEntity.notFound().build();
        }

        return entity;
    }

    @GetMapping(value = URL_GET_GALLERY, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModelsPage> getGallery(@PathVariable Long postId, HttpServletRequest request) throws BusinessException {
        ResponseEntity<ModelsPage> entity;
        Post openingPost = postService.findById(postId);
        if (openingPost != null && openingPost.isOpening()) {
            PostThread thread = openingPost.getThread();
            List<AttachmentModel> models = new ArrayList<>();

            List<Attachment> attachments = attachmentService.findByThread(thread);
            for (Attachment attachment : attachments) {
                models.add(new AttachmentModel(attachment.getFilename(),
                        attachment.getUrl(),
                        viewService.getType(attachment), viewService.getData(attachment)));
            }

            String shortTitle = postService.getShortTitle(openingPost);
            ModelsPage modelsPage = new ModelsPage(
                    new GalleryModel(openingPost.getId(), models));
            modelsPage.setTitle(shortTitle);
            entity = ResponseEntity.ok(modelsPage);
        } else {
            entity = ResponseEntity.notFound().build();
        }

        return entity;
    }

    @PostMapping(value = URL_PURGE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> purgeUser(@RequestParam Long postId) throws PermissionException {
        settingsService.validatePermission();

        Post post = postService.findById(postId);
        postService.purgeFromPost(post);

        banService.banUser(post.getPosterAddress(), "Post #" + post.getId());

        postService.purgeFromPost(post);

        return ResponseEntity.ok(new StatusResponse(AllThreadsController.URL_ALL_THREADS));
    }

    @PostMapping(URL_DELETE_POST)
    public ResponseEntity<StatusResponse> deletePost(@PathVariable Long postId) throws BusinessException {
        settingsService.validatePermission();

        StatusResponse response;

        Post post = postService.findById(postId);
        if (post == null) {
            throw new NotFoundException("¯\\_(ツ)_/¯");
        }

        if (post.isOpening()) {
            threadService.delete(post.getThread());
            response = new StatusResponse(AllThreadsController.URL_ALL_THREADS);
        } else {
            response = new StatusResponse();
            postService.delete(post);
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping(value = URL_THREADS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StatusResponse> createThread(@Valid ThreadForm form, BindingResult result, HttpServletRequest request) throws BusinessException {
        boolean valid = !result.hasErrors();

        ResponseEntity<StatusResponse> entity;
        StatusResponse response = new StatusResponse();
        if (!valid) {
            saveErrorMessages(result, response);

            entity = ResponseEntity.badRequest().body(response);
        } else {
            PostCreateDetails details = postService.getCreateDetails(form,
                    null, utilService.getIpFromRequest(request));

            Post post = postService.create(details);
            response.setUrl(postService.getUrl(post));

            entity = ResponseEntity.ok(response);
        }

        return entity;
    }

    @GetMapping(value = URL_SEARCH)
    public ResponseEntity<ModelsPage> search(@RequestParam String query,
                                             @RequestParam(required = false) Boolean threadsOnly,
                                             @RequestParam Optional<Integer> page,
                                             HttpServletRequest request) throws BusinessException {
        Pageable byPubTime = utilService.getPageable(page,
                postsPerPage, Sort.by(Sort.Order.desc(Post.PUB_TIME)));

        // TODO Search only OPs

        Page<Post> searchResult = postService.search(byPubTime, query);
        List<Post> posts = searchResult.getContent();
        List<Object> models = new ArrayList<>();

        List<Tag> tags = tagService.search(query);
        for (Tag tag : tags) {
            models.add(new TagModel(tag.getName()));
        }

        for (Post post : posts) {
            models.add(postService.getModel(post));
        }

        ModelsPage modelsPage = new ModelsPage(models, searchResult.getNumber(),
                searchResult.getTotalPages(), request.getRequestURI(), PostController.URL_SEARCH);
        modelsPage.setTitle(utilService.getMessage("common.search"));
        return ResponseEntity.ok(modelsPage);
    }

    @GetMapping(URL_FEED)
    public ResponseEntity<ModelsPage> feed(@RequestParam Optional<Integer> page,
                                           @RequestParam Optional<String> tripcode,
                                           @RequestParam Optional<String> address,
                                           @RequestParam Optional<String> filename,
                                           HttpServletRequest request) throws BusinessException {
        Post post = new Post();

        tripcode.ifPresent(post::setTripcode);

        if (address.isPresent() && settingsService.isAdmin()) {
            post.setPosterAddress(address.get());
        }

        Example<Post> example = Example.of(post);

        Page<Post> posts;
        Pageable byPubTime = utilService.getPageable(page,
                postsPerPage, Sort.by(Sort.Order.desc(Post.PUB_TIME)));

        if (filename.isPresent()) {
            posts = postService.findByAttachmentFilename(byPubTime, filename.get());
        } else {
            posts = postService.findByExample(byPubTime, example);
        }

        List<Object> models = new ArrayList<>(posts.getSize());
        for (Post feedPost : posts.getContent()) {
            models.add(postService.getModel(feedPost));
        }

        ModelsPage modelsPage = new ModelsPage(models, posts.getNumber(),
                posts.getTotalPages(), request.getRequestURI(), PostController.URL_FEED);
        modelsPage.setTitle(utilService.getMessage("common.feed"));
        return ResponseEntity.ok(modelsPage);
    }

    @GetMapping(URL_FEED_CRITERIA)
    public ResponseEntity<ModelsPage> feedCriteria(@RequestParam Optional<String> tripcode,
                                           @RequestParam Optional<String> address,
                                           @RequestParam Optional<String> filename) {
        List<Object> models = new ArrayList<>();
        if (tripcode.isPresent()) {
            models.add(new CriteriaModel(utilService.getMessage(CRITERIA_TRIPCODE), tripcode.get()));
        }

        if (settingsService.isAdmin() && address.isPresent()) {
            models.add(new CriteriaModel(utilService.getMessage(CRITERIA_ADDRESS), address.get()));
        }

        if (filename.isPresent()) {
            models.add(new CriteriaModel(utilService.getMessage(CRITERIA_ATTACHMENT), filename.get()));
        }


        return ResponseEntity.ok(new ModelsPage(models));
    }

    @GetMapping(value = {URL_THREADS, URL_TAG_THREADS})
    public ResponseEntity<ModelsPage> allThreads(@RequestParam Optional<Integer> page,
                                           @PathVariable Optional<String> tagName,
                                           HttpServletRequest request) throws BusinessException {
        Page<PostThread> threads;
        if (tagName.isPresent()) {
            Tag tag = tagService.findByName(tagName.get());

            if (tag == null) {
                return ResponseEntity.notFound().build();
            }

            threads = threadService.findByTag(tag, utilService.getPageable(page, threadsPerPage));
        } else {
            threads = threadService.findAll(
                    utilService.getPageable(page, threadsPerPage));
        }

        List<Object> models = new ArrayList<>();
        for (PostThread thread : threads.getContent()) {
            models.add(threadPostService.getModel(thread));
        }

        String webUrl = tagName.map(s -> AllThreadsController.URL_TAG + s + "/").orElse(AllThreadsController.URL_ALL_THREADS);
        return ResponseEntity.ok(new ModelsPage(models, threads.getNumber(),
                threads.getTotalPages(), request.getRequestURI(), webUrl));
    }

    @GetMapping(value = {URL_ACTIVE_THREADS, URL_TAG_ACTIVE_THREADS},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModelsPage> getActiveThreads(@PathVariable Optional<String> tagName) throws BusinessException {
        Tag tag = null;
        if (tagName.isPresent()) {
            tag = tagService.findByName(tagName.get());
            if (tag == null) {
                return ResponseEntity.notFound().build();
            }
        }
        List<Post> activeOps = postService.findTodayActiveThreads(tag);
        List<Object> models = new ArrayList<>();
        for (Post post : activeOps) {
            models.add(threadPostService.getRef(post));
        }
        return ResponseEntity.ok(new ModelsPage(models));
    }

    @GetMapping(value = URL_FORM, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModelsPage> getForm(@RequestParam Optional<String> title,
                                              @RequestParam Optional<String> action,
                                              @RequestParam Optional<Boolean> tags,
                                              @RequestParam Optional<Integer> fromId) {
        FormModel formModel = new FormModel(
                title.orElse(""),
                action.orElse(""),
                false,
                tags.orElse(false));

        if (fromId.isPresent()) {
            Post existingOpening = postService.findById(fromId.get());
            formModel.setTitle(existingOpening.getTitle() + " NEW");
            formModel.setTags(existingOpening.getThread().getTags().stream()
                    .map(Tag::getName)
                    .collect(Collectors.joining(ThreadForm.DELIMITER_FORM)));
            formModel.setText(Post.REFLINK_START + existingOpening.getId());
        }

        return ResponseEntity.ok(new ModelsPage(formModel));
    }

    @GetMapping(value = URL_SEARCH_FORM, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ModelsPage> getSearchForm(@RequestParam Optional<String> query) {
        return ResponseEntity.ok(new ModelsPage(
                new SearchFormModel(query.orElse(""), settingsService.isAdmin())));
    }

}
