package me.neboard.newneboard.attachment.creation;

import me.neboard.newneboard.attachment.creation.attachment.FileFormAttachment;
import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class FromDownloadFileCreator extends FromFileCreator implements AttachmentCreator<FileFormAttachment> {
    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FileService fileService;

    @Override
    public Attachment getOrCreate(FileFormAttachment source) throws BusinessException {
        File file = source.getFile();

        return createFromSource(file.getName(), source);
    }

    @Override
    public boolean handles(FormAttachment source) {
        return source instanceof FileFormAttachment;
    }

}
