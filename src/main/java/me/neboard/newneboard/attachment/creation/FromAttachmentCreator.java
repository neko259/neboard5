package me.neboard.newneboard.attachment.creation;

import me.neboard.newneboard.attachment.creation.attachment.AttachmentFormAttachment;
import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import org.springframework.stereotype.Component;

@Component
public class FromAttachmentCreator implements AttachmentCreator<AttachmentFormAttachment> {
    @Override
    public Attachment getOrCreate(AttachmentFormAttachment source) throws BusinessException {
        return source.getAttachment();
    }

    @Override
    public boolean handles(FormAttachment source) {
        return source instanceof AttachmentFormAttachment;
    }
}
