package me.neboard.newneboard.attachment.creation.attachment;

import me.neboard.newneboard.domain.Attachment;

public class AttachmentFormAttachment extends AbstractFormAttachment<Attachment> {
    public AttachmentFormAttachment(Attachment source) {
        super(source);
    }

    @Override
    public long getSize() {
        return 0;
    }

    public Attachment getAttachment() {
        return source;
    }
}
