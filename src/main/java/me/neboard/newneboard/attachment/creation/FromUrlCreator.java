package me.neboard.newneboard.attachment.creation;

import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.attachment.creation.attachment.UrlFormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FromUrlCreator implements AttachmentCreator<UrlFormAttachment> {
    @Autowired
    private AttachmentService attachmentService;

    @Override
    public Attachment getOrCreate(UrlFormAttachment source) {
        String url = source.getUrl();
        Attachment attachment = attachmentService.findByUrl(url);
        if (attachment == null) {
            attachment = new Attachment(null, url);
            attachment = attachmentService.save(attachment);
        }
        return attachment;

    }

    @Override
    public boolean handles(FormAttachment source) {
        return source instanceof UrlFormAttachment;
    }
}
