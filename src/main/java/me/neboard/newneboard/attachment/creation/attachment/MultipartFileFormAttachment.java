package me.neboard.newneboard.attachment.creation.attachment;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MultipartFileFormAttachment extends AbstractFormAttachment<MultipartFile> implements StreamableFormAttachment {

    public MultipartFileFormAttachment(MultipartFile source) {
        super(source);
    }

    @Override
    public long getSize() {
        return source.getSize();
    }

    public MultipartFile getMultipartFile() {
        return source;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new BufferedInputStream(source.getInputStream());
    }
}
