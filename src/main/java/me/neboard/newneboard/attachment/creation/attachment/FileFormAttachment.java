package me.neboard.newneboard.attachment.creation.attachment;

import java.io.*;

public class FileFormAttachment extends AbstractFormAttachment<File> implements StreamableFormAttachment {

    public FileFormAttachment(File source) {
        super(source);
    }

    @Override
    public long getSize() {
        return source.length();
    }

    public void deleteFile() {
        source.delete();
    }

    public File getFile() {
        return source;
    }

    @Override
    public InputStream getInputStream() throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(source));
    }
}
