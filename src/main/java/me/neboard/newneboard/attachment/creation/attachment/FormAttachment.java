package me.neboard.newneboard.attachment.creation.attachment;

public interface FormAttachment {
    long getSize();
}
