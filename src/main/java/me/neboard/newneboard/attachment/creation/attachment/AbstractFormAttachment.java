package me.neboard.newneboard.attachment.creation.attachment;

public abstract class AbstractFormAttachment<T> implements FormAttachment {
    protected T source;

    public AbstractFormAttachment(T source) {
        this.source = source;
    }
}
