package me.neboard.newneboard.attachment.creation.attachment;

import java.io.IOException;
import java.io.InputStream;

public interface StreamableFormAttachment {
    InputStream getInputStream() throws IOException;
}
