package me.neboard.newneboard.attachment.creation;

import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.attachment.creation.attachment.MultipartFileFormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FromMultipartFileCreator extends FromFileCreator implements AttachmentCreator<MultipartFileFormAttachment> {
    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FileService fileService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private ImageService imageService;

    @Override
    public Attachment getOrCreate(MultipartFileFormAttachment source) throws BusinessException {
        MultipartFile file = source.getMultipartFile();
        Attachment result = null;
        if (!file.isEmpty()) {
            result = createFromSource(file.getOriginalFilename(), source);
        }

        return result;
    }

    @Override
    public boolean handles(FormAttachment source) {
        return source instanceof MultipartFileFormAttachment;
    }

}
