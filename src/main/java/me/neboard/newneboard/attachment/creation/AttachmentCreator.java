package me.neboard.newneboard.attachment.creation;

import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

public interface AttachmentCreator<T extends FormAttachment> {
    Attachment getOrCreate(T source) throws BusinessException;
    boolean handles(FormAttachment source);
}
