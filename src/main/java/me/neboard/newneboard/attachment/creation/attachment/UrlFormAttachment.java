package me.neboard.newneboard.attachment.creation.attachment;

public class UrlFormAttachment extends AbstractFormAttachment<String> {

    public UrlFormAttachment(String source) {
        super(source);
    }

    @Override
    public long getSize() {
        return 0;
    }

    public String getUrl() {
        return source;
    }
}
