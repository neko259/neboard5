package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class VectorImageViewer extends ImageViewer {
	private static final String MIMETYPE_SVG = "image/svg+xml";

	@Value("${image.thumb.height}")
	private int thumbHeight;

	@Value("${image.thumb.width}")
	private int thumbWidth;

	@Autowired
	private FileService fileService;

	@Autowired
	private AttachmentService attachmentService;

	@Override
	public boolean handles(Attachment attachment) throws BusinessException {
		return attachment.isFileAttachment() && MIMETYPE_SVG.equals(
				fileService.getMimetype(attachment.getFilename()));
	}

	@Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
		Map<String, String> data = getGenericData(attachment);

		data.put(DATA_WIDTH, String.valueOf(thumbWidth));
		data.put(DATA_HEIGHT, String.valueOf(thumbHeight));
		data.put("id", String.valueOf(attachment.getId()));
		data.put(DATA_URL, ExternalImageController.URL_FILES + attachment.getFilename());

		return data;
	}

	@Override
	public String getType() {
		return "vector";
	}
}
