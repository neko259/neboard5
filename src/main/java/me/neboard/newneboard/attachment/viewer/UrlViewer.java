package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.ImageFileMetadata;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.web.ExternalImageController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UrlViewer extends FileViewer {
    private static final String URL_IMAGE = "/images/url.png";

    private static final Pattern REGEX_URL = Pattern.compile("(\\w*://)([\\w-_.]+)([:\\w\\W]*)");

    @Value("${url.max.width}")
    private int maxUrlWidth;

    @Autowired
    private FileService fileService;

    @Autowired
    private ImageService imageService;

    @Override
    public boolean handles(Attachment attachment) {
        return attachment.isUrlAttachment();
    }

    @Override
    protected String getGenericImage() throws BusinessException {
        return URL_IMAGE;
    }

    @Override
    protected String getMetadata(Attachment attachment) {
        return HtmlUtils.htmlEscape(
                StringUtils.abbreviate(attachment.getUrl(), maxUrlWidth));
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = super.getData(attachment);
        String host = null;
        try {
            host = getDomain(attachment.getUrl());
        } catch (BusinessException e) {
            // Just set to null and process later
        }

        boolean domainImageFound = false;
        if (host != null) {
            ExternalImageData imageData = imageService.getDomainImage(host);

            if (imageData != null && imageData.hasData()) {
                data.put(DATA_URL, ExternalImageController.URL_FILES + imageData.getName());
                ImageFileMetadata metadata = fileService.getMetadata(imageData.getName(), ImageFileMetadata.class);
                data.put(DATA_WIDTH, String.valueOf(metadata.getWidth()));
                data.put(DATA_HEIGHT, String.valueOf(metadata.getHeight()));

                domainImageFound = true;
            }
        }

        if (!domainImageFound) {
            data.putAll(super.getData(attachment));
        }

        return data;
    }

    public String getDomain(String url) throws BusinessException {
        Matcher m = REGEX_URL.matcher(url);
        if (m.matches()) {
            return m.group(2);
        } else {
            throw new BusinessException("Invalid URL");
        }
    }

    @Override
    public String getType() {
        return "url";
    }
}
