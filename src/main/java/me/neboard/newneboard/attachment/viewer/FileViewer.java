package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.ImageFileMetadata;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.ExternalService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FileViewer extends AbstractViewer {
    private static final String FORMAT_FILE_NAME = "/images/file.png";

    @Autowired
    private FileService fileService;

    @Autowired
    private ExternalService externalService;

    @Autowired
    private ImageService imageService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return false;
    }

    protected String getGenericImage() throws BusinessException {
        return FORMAT_FILE_NAME;
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = super.getData(attachment);

        ExternalImageData imageData = null;

        boolean dataFound = false;
        if (attachment.isFileAttachment()) {
            String mimetype = fileService.getMimetype(attachment.getFilename());
            if (mimetype == null) {
                data.putAll(MissingFileViewer.getMissingFileData(imageService, attachment));
                dataFound = true;
            } else {
                imageData = imageService.getFileImage(mimetype);
            }
        }

        String imageName;
        long width;
        long height;

        if (!dataFound) {
            if (imageData == null || !imageData.hasData()) {
                String genericImageName = getGenericImage();
                Dimensions imageSize = imageService.getStaticImageSizeFromClasspath(genericImageName);

                imageName = genericImageName;
                width = imageSize.getWidth();
                height = imageSize.getHeight();
            } else {
                ImageFileMetadata metadata = fileService.getMetadata(imageData.getName(), ImageFileMetadata.class);

                imageName = ExternalImageController.URL_FILES + imageData.getName();
                width = metadata.getWidth();
                height = metadata.getHeight();
            }

            data.put(DATA_URL, imageName);
            data.put(DATA_WIDTH, String.valueOf(width));
            data.put(DATA_HEIGHT, String.valueOf(height));
        }

        return data;
    }

    @Override
    public String getType() {
        return "file";
    }

}
