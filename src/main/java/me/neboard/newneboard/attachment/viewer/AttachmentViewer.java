package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

import java.util.Map;

public interface AttachmentViewer {
    String DATA_URL = "url";
    String DATA_WIDTH = "width";
    String DATA_HEIGHT = "height";

    boolean handles(Attachment attachment) throws BusinessException;

    Map<String, String> getData(Attachment attachment) throws BusinessException;
    String getType();
}
