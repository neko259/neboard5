package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class AudioViewer extends FileViewer {
    @Value("${media.types.audio}")
    private Set<String> audioMimetypes;

    @Autowired
    private UtilService utilService;

    @Autowired
    private FileService fileService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return attachment.isFileAttachment() && isAudio(
                fileService.getMimetype(attachment.getFilename()));
    }

    private boolean isAudio(String mimetype) {
        return audioMimetypes.contains(mimetype);
    }

    @Override
    public String getType() {
        return "audio";
    }
}
