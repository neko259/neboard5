package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class AbstractViewer implements AttachmentViewer {
    private static final String HTML_METADATA = "<a href='%s' download=''>%s, %s</a> <a href='/feed/?filename=%s'>&#10697;</a>";

    private static final String DATA_METADATA = "metadata";
    private static final String DATA_FILE_URL = "fileUrl";
    private static final String DATA_LINK_ATTRS = "linkAttrs";

    @Autowired
    private FileService fileService;

    @Autowired
    private AttachmentService attachmentService;

    protected String getMetadata(Attachment attachment) throws BusinessException {
        String mimetype = fileService.getMimetype(attachment.getFilename());
        if (mimetype == null) {
            mimetype = "unknown";
        }
        return String.format(HTML_METADATA,
                attachmentService.getUrl(attachment),
                mimetype,
                FileUtils.byteCountToDisplaySize(fileService.getFileSize(attachment.getFilename())),
                attachment.getFilename());
    }

    protected String getLinkAttributes() {
        return "";
    }

    private String getFileUrl(Attachment attachment) {
        return attachmentService.getUrl(attachment);
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        return new HashMap<>(getGenericData(attachment));
    }

    protected Map<String, String> getGenericData(Attachment attachment) throws BusinessException {
        Map<String, String> data = new HashMap<>();
        data.put(DATA_METADATA, getMetadata(attachment));
        data.put(DATA_FILE_URL, getFileUrl(attachment));
        data.put(DATA_LINK_ATTRS, getLinkAttributes());
        return data;
    }

}
