package me.neboard.newneboard.attachment.viewer;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public class VideoViewer extends AbstractViewer {
    private static final String MIMETYPE_VIDEO = "video/";

    @Value("${media.types.video}")
    private Set<String> videoMimetypes;

    @Autowired
    private FileService fileService;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    public boolean handles(Attachment attachment) throws BusinessException {
        return attachment.isFileAttachment() && isVideo(
                fileService.getMimetype(attachment.getFilename()));
    }

    private boolean isVideo(String mimetype) {
        return videoMimetypes.contains(mimetype) || (mimetype != null && mimetype.startsWith(MIMETYPE_VIDEO));
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        Map<String, String> data = super.getData(attachment);
        data.put(DATA_URL, ExternalImageController.URL_FILES + attachment.getFilename());
        return data;
    }

    @Override
    public String getType() {
        return "video";
    }
}
