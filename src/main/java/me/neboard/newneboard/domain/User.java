package me.neboard.newneboard.domain;


import jakarta.persistence.*;
import org.hibernate.annotations.SortNatural;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(name = "buser")
public class User implements PersistentEntity {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String uuid;

    @Column
    private String theme;

    @Column
    private String parseMethod;

    @Column
    private String imageMode;

    @Column
    private String timezone;

    @Column
    private Date lastOperatedTime;

    @Column
    private Date lastPostTime;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "user_fav_tag",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    @OrderBy("name")
    @SortNatural
    private SortedSet<Tag> favoriteTags;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "user_hid_tag",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    @OrderBy("name")
    @SortNatural
    private SortedSet<Tag> hiddenTags;

    @Override
    public Long getId() {
        return id;
    }

    public User(String uuid) {
        this.uuid = uuid;
    }

    public User() {
    }

    public String getUuid() {
        return uuid;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getImageMode() {
        return imageMode;
    }

    public void setImageMode(String imageMode) {
        this.imageMode = imageMode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Date getLastOperatedTime() {
        return lastOperatedTime;
    }

    public void setLastOperatedTime(Date lastOperatedTime) {
        this.lastOperatedTime = lastOperatedTime;
    }

    public Set<Tag> getFavoriteTags() {
        return favoriteTags == null ? new HashSet<>() : favoriteTags;
    }

    @Override
    public String toString() {
        return uuid;
    }

    public Set<Tag> getHiddenTags() {
        return hiddenTags == null ? new HashSet<>() : hiddenTags;
    }

    public Date getLastPostTime() {
        return lastPostTime;
    }

    public String getParseMethod() {
        return parseMethod;
    }

    public void setParseMethod(String parseMethod) {
        this.parseMethod = parseMethod;
    }

    public void setLastPostTime(Date lastPostTime) {


        this.lastPostTime = lastPostTime;
    }
}
