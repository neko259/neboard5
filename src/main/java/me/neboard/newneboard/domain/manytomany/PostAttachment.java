package me.neboard.newneboard.domain.manytomany;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.Post;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Entity
public class PostAttachment implements Comparable<PostAttachment>, Serializable {
    @Id
    @ManyToOne
    @JoinColumn
    private Post post;

    @Id
    @ManyToOne
    @JoinColumn
    private Attachment attachment;

    @Column(name = "PA_ORDER")
    private Integer order;

    public PostAttachment(Attachment attachment, int order) {
        this.attachment = attachment;
        this.order = order;
    }

    public PostAttachment() {
        // Default constructor for jpa
    }

    public Post getPost() {
        return post;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public Integer getOrder() {
        return order;
    }

    @Override
    public int compareTo(PostAttachment postAttachment) {
        int result;
        if (getOrder() == null || postAttachment.getOrder() == null) {
            result = Objects.compare(getAttachment().getId(), postAttachment.getAttachment().getId(), Comparator.naturalOrder());
        } else {
            result = Objects.compare(getOrder(), postAttachment.getOrder(), Comparator.naturalOrder());
        }
        return result;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
