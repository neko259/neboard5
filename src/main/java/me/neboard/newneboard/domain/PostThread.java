package me.neboard.newneboard.domain;

import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Where;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name = "thread")
public class PostThread implements PersistentEntity {

    private static final String CLASS_DELIMITER = " ";

    public enum Status {
        ACTIVE,
        BUMPLIMIT,
        ARCHIVED
    }

    @Column
    private Date bumpTime;

    @Column
    private int postLimit;

    @Column
    private Status status;

    @Id
    @Column
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "thread", cascade={CascadeType.ALL})
    @OrderBy("id")
    @SortNatural
    private List<Post> posts;

    @OneToMany(mappedBy = "thread", cascade={CascadeType.ALL})
    @Where(clause = "opening = true")
    private List<Post> openingPosts;

    @ManyToMany(cascade={CascadeType.PERSIST, CascadeType.REFRESH})
    @OrderBy("name")
    @SortNatural
    private SortedSet<Tag> tags;

    public PostThread(Set<Tag> tags, int postLimit) {
        this.tags = new TreeSet<>(tags);
        this.status = Status.ACTIVE;
        this.postLimit = postLimit;
    }

    public PostThread() {
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Date getBumpTime() {
        return bumpTime;
    }

    public int getPostLimit() {
        return postLimit;
    }

    public Status getStatus() {
        return status;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public Post getOpeningPost() {
        return openingPosts.get(0);
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return !hasPosts() ? String.valueOf(getId()) : getId() + "/" + getOpeningPost().getId();
    }

    public boolean hasPosts() {
        return !posts.isEmpty();
    }

    public boolean hasReplies() {
        return !getPosts().isEmpty();
    }

    public void setBumpTime(Date bumpTime) {
        this.bumpTime = bumpTime;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean canPost() {
        return Status.ARCHIVED != getStatus();
    }

}
