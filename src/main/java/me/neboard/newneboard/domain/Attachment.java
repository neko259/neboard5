package me.neboard.newneboard.domain;

import me.neboard.newneboard.domain.manytomany.PostAttachment;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.SortNatural;

import jakarta.persistence.*;
import java.util.SortedSet;

@Entity
@Table(name = "attachment")
public class Attachment implements Comparable<Attachment> {

    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private String filename;

    @Column
    private String url;

    @OneToMany(mappedBy = "attachment", cascade = CascadeType.ALL)
    @OrderBy("order")
    @SortNatural
    private SortedSet<PostAttachment> postAttachments;

    public Attachment(String filename, String url) {
        this.filename = filename;
        this.url = url;

        if (StringUtils.isNotBlank(filename) && StringUtils.isNotBlank(url)) {
            throw new UnsupportedOperationException("Attachment cannot have both url and file name");
        }
    }

    public Attachment() {
    }

    public String getFilename() {
        return filename;
    }

    public SortedSet<PostAttachment> getPostAttachments() {
        return postAttachments;
    }

    public String getUrl() {
        return url;
    }

    public boolean isUrlAttachment() {
        return !StringUtils.isBlank(getUrl());
    }

    public boolean isFileAttachment() {
        return !isUrlAttachment();
    }

    public Long getId() {
        return id;
    }

    @Override
    public int compareTo(Attachment attachment) {
        return getId().compareTo(attachment.getId());
    }

    @Override
    public String toString() {
        return filename != null ? filename : url;
    }

    public void setFilename(String filename) {
        this.url = null;
        this.filename = filename;
    }
}
