package me.neboard.newneboard.domain;

import jakarta.persistence.*;
import java.util.List;

@Entity
@Table(name = "tag")
public class Tag implements Comparable<Tag>, PersistentEntity {
    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    @ManyToMany(mappedBy = "tags", cascade={CascadeType.ALL})
    private List<PostThread> threads;

    public Tag(String name) {
        this.name = name;
    }

    public Tag() {
    }

    public String getName() {
        return name;
    }

    public List<PostThread> getThreads() {
        return threads;
    }

    @Override
    public int compareTo(Tag tag) {
        return getName().compareTo(tag.getName());
    }

    @Override
    public Long getId() {
        return id;
    }

    public static Tag from(String name) {
        return new Tag(name.trim().toLowerCase());
    }
}
