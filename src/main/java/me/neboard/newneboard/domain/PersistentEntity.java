package me.neboard.newneboard.domain;

public interface PersistentEntity {
    String DELIMITER = ", ";


    Long getId();
}
