package me.neboard.newneboard.domain;

import me.neboard.newneboard.domain.manytomany.PostAttachment;
import org.hibernate.annotations.SortNatural;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name = "post")
public class Post implements PersistentEntity, Comparable<Post> {
    public static final String REFLINK_START = ">>";
    public static final String PUB_TIME = "pubTime";

    @Id
    @Column
    @GeneratedValue(strategy=GenerationType.TABLE)
    private Long id;

    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REFRESH})
    private PostThread thread;

    @Column
    private String title;

    @Column(columnDefinition="text")
    private String text;

    @Column(columnDefinition="text")
    private String rawText;

    @Column
    private Date pubTime;

    @Column
    private Boolean opening;

    @Column
    private String uuid;

    @Column
    private String posterAddress;

    @Column
    private String tripcode;

    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    @OrderBy("order")
    @SortNatural
    private List<PostAttachment> postAttachments;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "post_reply",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "reply_id")
    )
    @OrderBy("id")
    @SortNatural
    private SortedSet<Post> replies;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "post_reply",
            joinColumns = @JoinColumn(name = "reply_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id")
    )
    private Set<Post> referencedPosts = new HashSet<>();

    public Post(String title, String rawText, String text, Date pubTime, PostThread thread, boolean opening,
                List<PostAttachment> postAttachments, String posterAddress, String tripcode) {
        this.title = title;
        this.text = text;
        this.thread = thread;
        this.opening = opening;
        this.postAttachments = postAttachments;
        for (PostAttachment postAttachment : postAttachments) {
            postAttachment.setPost(this);
        }
        this.rawText = rawText;
        this.pubTime = pubTime;
        this.posterAddress = posterAddress;
        this.tripcode = tripcode;
    }

    public Post() {
    }

    public PostThread getThread() {
        return thread;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Collection<PostAttachment> getPostAttachments() {
        return postAttachments;
    }

    public Collection<Post> getReplies() {
        return replies;
    }

    public Date getPubTime() {
        return pubTime;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Boolean isOpening() {
        return opening;
    }

    public String getRawText() {
        return rawText;
    }

    @Override
    public String toString() {
        return "#" + getId();
    }

    @Override
    public int compareTo(Post post) {
        return getId().compareTo(post.getId());
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public String getTripcode() {
        return tripcode;
    }

    public void setTripcode(String tripcode) {
        this.tripcode = tripcode;
    }

    public String getPosterAddress() {
        return posterAddress;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPosterAddress(String posterAddress) {
        this.posterAddress = posterAddress;
    }

    public Set<Post> getReferencedPosts() {
        return referencedPosts;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
