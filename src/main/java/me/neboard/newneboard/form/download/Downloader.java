package me.neboard.newneboard.form.download;

import me.neboard.newneboard.exception.BusinessException;

public interface Downloader {
    Object download(String url) throws BusinessException;

    boolean handles(String url);
}
