package me.neboard.newneboard.form.download;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class UrlDownloader implements Downloader {
    private static final Pattern REGEX_URL = Pattern.compile("^https?://.+$");

    @Autowired
    private FileService fileService;

    public Object download(String url) throws BusinessException {
        fileService.download(url);
        return url;
    }

    @Override
    public boolean handles(String url) {
        return REGEX_URL.matcher(url).matches();
    }

}
