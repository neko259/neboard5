package me.neboard.newneboard.form;

public class ThreadForm extends PostForm {
    public static final String DELIMITER_FORM = " ";

    private String tags;

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Override
    public String[] getTagNames() {
        return getTags().split(DELIMITER_FORM);
    }
}
