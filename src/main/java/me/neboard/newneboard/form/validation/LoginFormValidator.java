package me.neboard.newneboard.form.validation;

import me.neboard.newneboard.form.LoginForm;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginFormValidator implements Validator {
    private static final String ERROR_USER = "error.user";
    private static final String FIELD_USER = "user";

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return LoginForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LoginForm form = (LoginForm) target;

        if (userService.findByUuid(form.getUser()) == null) {
            errors.rejectValue(FIELD_USER, ERROR_USER);
        }
    }
}
