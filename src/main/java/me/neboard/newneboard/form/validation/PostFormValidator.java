package me.neboard.newneboard.form.validation;

import me.neboard.newneboard.attachment.creation.attachment.FileFormAttachment;
import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.form.PostForm;
import me.neboard.newneboard.form.download.Downloader;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.user.UserService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Component
public class PostFormValidator implements Validator {
	private static final String ERROR_TEXT_OR_ATTACHMENT_REQUIRED = "error.textOrAttachment.required";
	private static final String ERROR_TEXT_LENGTH = "error.text.length";
	private static final String ERROR_ATTACHMENTS_COUNT = "error.attachments.count";
	private static final String ERROR_ATTACHMENTS_SIZE = "error.attachments.size";
	private static final String ERROR_SPEED = "error.speed";
	private static final String ERROR_DOWNLOAD = "error.download";

	private static final String FIELD_ATTACHMENTS = "attachments";
	private static final String FIELD_TEXT = "text";

	private static final String REGEX_ATTACHMENTS_DELIMITER = "\r?\n";

	@Value("${text.max.length}")
    private int textMaxLength;

    @Value("${attachments.max.count}")
    private int maxAttachmentsCount;

    @Value("${attachments.total.size}")
	private long maxSize;

    @Value("${max.speed.seconds}")
	private long maxSpeedSeconds;

	@Autowired
	private FileService fileService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private UserService userService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private ExternalService externalService;

	@Autowired
	private List<Downloader> downloaders;

	@Autowired
	private FormAttachmentService formAttachmentService;

    @Override
	public boolean supports(Class<?> clazz) {
		return PostForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PostForm form = (PostForm) target;

		if (StringUtils.isBlank(form.getText()) && !hasAttachments(form)) {
			errors.rejectValue(FIELD_TEXT, ERROR_TEXT_OR_ATTACHMENT_REQUIRED);
		}

		if (form.getText().length() > textMaxLength) {
		    errors.rejectValue(FIELD_TEXT, ERROR_TEXT_LENGTH);
        }

		try {
			processAttachments(errors, form);
		} catch (BusinessException e) {
			errors.reject(ERROR_DOWNLOAD);
		}

		if (getAttachmentCount(form) > maxAttachmentsCount) {
			errors.rejectValue(FIELD_ATTACHMENTS, ERROR_ATTACHMENTS_COUNT);
		}

		validateSize(errors, form);

		if (!errors.hasErrors() && maxSpeedSeconds > 0) {
			// No need to validate speed if there are some errors user wants to correct
			validateSpeed(errors);
		}

		deleteTempFiles(errors, form);
	}

	private void deleteTempFiles(Errors errors, PostForm form) {
		if (errors.hasErrors()) {
			form.getProcessedAttachments().stream()
					.filter(o -> o instanceof FileFormAttachment)
					.forEach(o -> ((FileFormAttachment) o).deleteFile());
		}
	}

	private boolean validateSize(Errors errors, PostForm form) {
		boolean valid = true;

		boolean alreadyValidated = errors.getFieldErrors(FIELD_ATTACHMENTS)
				.stream()
				.anyMatch(error -> ERROR_ATTACHMENTS_SIZE.equals(error.getCode()));
		// Don't duplicate the same error for the same field twice
		if (!alreadyValidated) {
			long totalSize = form.getProcessedAttachments().stream()
					.mapToLong(FormAttachment::getSize)
					.sum();

			if (totalSize > maxSize) {
				valid = false;
				errors.rejectValue(FIELD_ATTACHMENTS, ERROR_ATTACHMENTS_SIZE);
			}
		} else {
			valid = false;
		}

		return valid;
	}

	public int getMaxAttachmentsCount() {
		return maxAttachmentsCount;
	}

	public String getMaxSizeStr() {
		return FileUtils.byteCountToDisplaySize(maxSize);
	}

	private boolean hasAttachments(PostForm form) {
		boolean result = !StringUtils.isEmpty(form.getAttachment_urls());

		if (!result && !form.getAttachments().isEmpty()) {
			for (MultipartFile file : form.getAttachments()) {
				if (!file.isEmpty()) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	private int getAttachmentCount(PostForm form) {
		return form.getProcessedAttachments().size();
	}

	private void validateSpeed(Errors errors) {
		User user = userService.getUser();
		Date lastPostDate = user.getLastPostTime();

		Instant now = Instant.now();
		Instant allowedDate = now.minusSeconds(maxSpeedSeconds);
    	if (lastPostDate == null) {
    		errors.reject(ERROR_SPEED, new Object[] {maxSpeedSeconds}, null);
			user.setLastPostTime(Date.from(now));
			userService.save(user);
		} else if (lastPostDate.getTime() / 1000 - allowedDate.getEpochSecond() > 0) {
			long secondsToWait = lastPostDate.getTime() / 1000 - allowedDate.getEpochSecond();
			errors.reject(ERROR_SPEED, new Object[] {secondsToWait}, null);
		} else {
    		user.setLastPostTime(Date.from(now));
			userService.save(user);
		}
	}

	private void processAttachments(Errors errors, PostForm form) throws BusinessException {
    	if (!StringUtils.isBlank(form.getAttachment_urls())) {
			for (String url : form.getAttachment_urls().split(REGEX_ATTACHMENTS_DELIMITER)) {
				if (StringUtils.isBlank(url)) {
					continue;
				}

				boolean handled = false;
				for (Downloader downloader : downloaders) {
					if (downloader.handles(url)) {
						Object download = downloader.download(url);
						form.addProcessedAttachment(formAttachmentService.getAttachment(download));
						handled = true;
					}
				}

				if (!handled) {
					errors.rejectValue(FIELD_ATTACHMENTS, ERROR_DOWNLOAD);
				}

				// Revalidate in case we are already downloading too much
				if (!validateSize(errors, form)) {
					break;
				}
			}
		}

    	if (form.getAttachments() != null) {
			form.getAttachments().stream()
					.map(formAttachmentService::getAttachment)
					.forEach(form::addProcessedAttachment);
			validateSize(errors, form);
		}
	}

}
