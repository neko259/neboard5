package me.neboard.newneboard.form;

import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.exception.BusinessException;

import java.util.List;

/**
 * From this form, a post can be created.
 */
public interface CreatableForm {
    String getTripcode() throws BusinessException;
    String getTitle();
    String getText();
    List<FormAttachment> getProcessedAttachments();

    String[] getTagNames();
}
