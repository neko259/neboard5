package me.neboard.newneboard.config;

import me.neboard.newneboard.filter.UserAwareFilter;
import me.neboard.newneboard.rest.ApiRestController;
import me.neboard.newneboard.rest.PostRestController;
import me.neboard.newneboard.web.AdminController;
import me.neboard.newneboard.web.AllThreadsController;
import me.neboard.newneboard.web.PostController;
import me.neboard.newneboard.web.ThreadController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfiguration {
    private static final String PATTERN_ALL = "*";

    @Autowired
    private UserAwareFilter userAwareFilter;

    @Bean
    public FilterRegistrationBean<UserAwareFilter> postAwareRegistration() {
        FilterRegistrationBean<UserAwareFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(userAwareFilter);
        registration.addUrlPatterns(AllThreadsController.URL_ALL_THREADS);
        registration.addUrlPatterns(AllThreadsController.URL_TAG + PATTERN_ALL);
        registration.addUrlPatterns(ThreadController.URL_PREFIX_THREAD + PATTERN_ALL);
        registration.addUrlPatterns(PostController.URL_FEED + PATTERN_ALL);
        registration.addUrlPatterns(PostController.URL_SEARCH + PATTERN_ALL);
        registration.addUrlPatterns(PostRestController.URL_THREAD_DIFF + PATTERN_ALL);
        registration.addUrlPatterns(ApiRestController.URL + PATTERN_ALL);
        registration.addUrlPatterns(AdminController.URL + PATTERN_ALL);
        return registration;
    }
}
