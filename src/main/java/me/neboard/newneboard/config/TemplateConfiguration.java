package me.neboard.newneboard.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.spring6.expression.ThymeleafEvaluationContext;

@Configuration
public class TemplateConfiguration {
    @Autowired
    ApplicationContext applicationContext;

    @Bean
    public ThymeleafEvaluationContext thymeleafContext() {
        return new ThymeleafEvaluationContext(applicationContext, null);
    }

}
