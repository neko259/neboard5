package me.neboard.newneboard.exception;

public class FileNotFoundException extends BusinessException {
    public FileNotFoundException(String message) {
        super(message);
    }

    public FileNotFoundException(Throwable cause) {
        super(cause);
    }
}
