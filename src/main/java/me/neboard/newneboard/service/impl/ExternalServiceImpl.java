package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.abstracts.ExternalCacheMeta;
import me.neboard.newneboard.abstracts.TimeResult;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.ExternalRequestException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.parser.ExternalParser;
import me.neboard.newneboard.service.CacheService;
import me.neboard.newneboard.service.ExternalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import jakarta.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ExternalServiceImpl implements ExternalService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalServiceImpl.class);

    @Value("${external.parser.host}")
    private String parserServiceHost;

    @Value("${external.file.host}")
    private String fileServiceHost;

    @Autowired
    private CacheService cacheService;

    private final List<ExternalCacheMeta> caches = new ArrayList<>();

    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    private void init() {
        caches.add(new ExternalCacheMeta(CACHE_DOMAIN_IMAGE));
        caches.add(new ExternalCacheMeta(CACHE_FILE_IMAGE));
        caches.add(new ExternalCacheMeta(ExternalParser.CACHE_PARSER_METHODS, getParserServiceUrl() + "/last-update"));
    }

    @Override
    public String getParserServiceUrl() {
        return parserServiceHost;
    }

    @Override
    public String getFileServiceUrl() {
        return fileServiceHost;
    }

    @Override
    public String getFileServiceUrl(String relativeUrl) {
        return concatUrl(getFileServiceUrl(), relativeUrl);
    }

    @Override
    public <T> T requestPost(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException {
        return request(url, HttpMethod.POST, data, null, resultClass);
    }

    @Override
    public <T> T requestGet(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException {
        return request(url, HttpMethod.GET, data, null, resultClass);
    }

    @Override
    public <T> T request(String url, HttpMethod method, Map<String, Object> data, HttpHeaders headers, Class<T> resultClass) throws BusinessException {
        MultiValueMap<String, Object> formData = null;
        if (data != null && !data.isEmpty()) {
            formData = new LinkedMultiValueMap<>();
            data.forEach(formData::add);
        }

        HttpEntity<Object> requestEntity = null;
        if (formData != null) {
            requestEntity = new HttpEntity<>(formData, headers);
        }
        return getObjectFromRequest(url, method, requestEntity, resultClass);
    }

    @Override
    public String rawRequest(String url, HttpMethod method, Object body, HttpHeaders headers) throws BusinessException {
        HttpEntity<Object> requestEntity = null;
        if (body != null) {
            requestEntity = new HttpEntity<>(body, headers);
        }
        return getStringFromRequest(url, method, requestEntity);
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void clearExternalCaches() throws BusinessException {
        for (ExternalCacheMeta cacheMeta : caches) {
            if (cacheMeta.isTimeSupported()) {
                TimeResult response = requestGet(cacheMeta.getCacheUpdateUrl(), null, TimeResult.class);
                if (response != null) {
                    String time = response.getTime();
                    if (time != null && !time.equals(cacheMeta.getCacheUpdateTime())) {
                        LOGGER.info(String.format("Clearing external cache %s as the update time has changed", cacheMeta.getCacheName()));
                        cacheService.clear(cacheMeta.getCacheName());
                        cacheMeta.setCacheUpdateTime(time);
                    }
                }
            } else {
                cacheService.clear(cacheMeta.getCacheName());
            }
        }
    }

    @Override
    public String concatUrl(String parentUrl, String childUrl) {
        return parentUrl.endsWith("/") ? parentUrl + childUrl : parentUrl + "/" + childUrl;
    }


    private String getStringFromRequest(String url, HttpMethod method, HttpEntity requestEntity) throws BusinessException {
        return getObjectFromRequest(url, method, requestEntity, String.class);
    }

    private <T> T getObjectFromRequest(String url, HttpMethod method, HttpEntity requestEntity,
                                       Class<T> responseType) throws NotFoundException, ExternalRequestException {
        ResponseEntity<T> response;
        try {
            response = restTemplate.exchange(url, method, requestEntity,
                    responseType);
        } catch (HttpClientErrorException.NotFound e) {
            throw new NotFoundException("Nothing found from request " + url);
        } catch (HttpClientErrorException | ResourceAccessException e) {
            throw new ExternalRequestException(e);
        }

        return response.getBody();
    }

}
