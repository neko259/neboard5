package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.rest.object.PostModel;
import me.neboard.newneboard.rest.object.ThreadPreviewModel;
import me.neboard.newneboard.rest.object.ThreadRef;
import me.neboard.newneboard.service.PostService;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.ThreadPostService;
import me.neboard.newneboard.service.post.ThreadConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ThreadPostServiceImpl implements ThreadPostService {
    @Autowired
    private PostService postService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private ThreadConfigService threadConfigService;

    @Override
    public ThreadRef getRef(PostThread thread) {
        Post openingPost = thread.getOpeningPost();
        return getRef(openingPost);
    }

    @Override
    public ThreadRef getRef(Post openingPost) {
        return new ThreadRef(openingPost.getId(),
                postService.getUrl(openingPost),
                postService.getShortTitle(openingPost));
    }

    @Override
    public ThreadPreviewModel getModel(PostThread thread) throws BusinessException {
        Post openingPost = thread.getOpeningPost();
        Collection<Post> lastPosts = postService.getThreadLastPosts(thread);
        List<PostModel> lastPostModels = new ArrayList<>();
        for (Post post : lastPosts) {
            lastPostModels.add(postService.getModel(post, openingPost));
        }
        return new ThreadPreviewModel(postService.getModel(openingPost),
                lastPostModels, getSkippedRepliesCount(thread));
    }

    @Override
    public int getSkippedRepliesCount(PostThread thread) {
        return Math.max(postService.getCountByThread(thread) - threadConfigService.getLastPostCount() - 1, 0);
    }


}
