package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;

import java.util.List;

public class PostCreateDetails {
    private String title;
    private String text;
    private List<Attachment> attachments;
    private PostThread thread;
    private String[] tagNames;
    private String posterAddress;
    private String tripcode;

    public PostCreateDetails(String title, String text,
                             List<Attachment> attachments, String[] tagNames,
                             String posterAddress, String tripcode) {
        this.title = title;
        this.text = text;
        this.attachments = attachments;
        this.tagNames = tagNames;
        this.posterAddress = posterAddress;
        this.tripcode = tripcode;
    }

    public PostCreateDetails(String title, String text, List<Attachment> attachments, PostThread thread, String posterAddress, String tripcode) {
        this.title = title;
        this.text = text;
        this.attachments = attachments;
        this.thread = thread;
        this.posterAddress = posterAddress;
        this.tripcode = tripcode;
    }

    public PostCreateDetails() {
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public PostThread getThread() {
        return thread;
    }

    public String[] getTagNames() {
        return tagNames;
    }

    public String getPosterAddress() {
        return posterAddress;
    }

    public String getTripcode() {
        return tripcode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setThread(PostThread thread) {
        this.thread = thread;
    }

    public void setTagNames(String[] tagNames) {
        this.tagNames = tagNames;
    }

    public void setPosterAddress(String posterAddress) {
        this.posterAddress = posterAddress;
    }

    public void setTripcode(String tripcode) {
        this.tripcode = tripcode;
    }

    public boolean isOpening() {
        return thread == null;
    }

}
