package me.neboard.newneboard.service.impl;

import com.google.gson.Gson;
import me.neboard.newneboard.service.JsonService;
import org.springframework.stereotype.Service;

@Service
public class GsonService implements JsonService {
    private final Gson gson = new Gson();

    @Override
    public String toJson(Object obj) {
        return gson.toJson(obj);
    }

    @Override
    public <T> T fromJson(String json, Class<T> type) {
        T result = null;
        if (json != null) {
            result = gson.fromJson(json, type);
        }
        return result;
    }
}
