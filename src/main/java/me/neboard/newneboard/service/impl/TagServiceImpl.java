package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.repository.TagRepository;
import me.neboard.newneboard.rest.object.TagModel;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.TagService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service("tagService")
@Transactional
public class TagServiceImpl implements TagService {
    private static final String URL_TAG = "/tag/";
    private static final String HTML_LINK = "<a class='tag' href='%s'>%s</a>";

    private static final Pattern REGEX_TAGS = Pattern.compile("^[\\w\\s\\d\']+$");

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private SettingsService settingsService;

    @Override
    public Tag findByName(String name) {
        return tagRepository.findByName(name);
    }

    private Tag getOrCreate(Tag tag) {
        Tag existing = findByName(tag.getName());
        if (existing == null) {
            existing = tagRepository.save(tag);
        }
        return existing;
    }

    @Override
    public Tag getOrCreate(String name) {
        return getOrCreate(Tag.from(name));
    }

    @Override
    public List<Tag> findStartingWith(String name) {
        return tagRepository.findByNameStartsWith(name);
    }

    @Override
    public Page<Tag> search(Pageable pageable, String query) {
        Page<Tag> page;
        if (StringUtils.isBlank(query)) {
            page = new PageImpl<>(Collections.emptyList());
        } else {
            page = tagRepository.findByNameContains(pageable, query);
        }
        return page;
    }

    @Override
    public Set<Tag> getTagsFromNames(String[] tagNames) {
        return Arrays.stream(tagNames)
                .filter(StringUtils::isNotBlank)
                .map(this::getOrCreate)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isValidName(String tagName) {
        Matcher tagMatcher = REGEX_TAGS.matcher(tagName);
        return tagMatcher.matches();
    }

    @Override
    public String getUrl(Tag tag) {
        String tagName;
        try {
            tagName = URLEncoder.encode(tag.getName(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            tagName = tag.getName();
        }
        return URL_TAG + tagName;
    }

    @Override
    public String getHtmlLink(Tag tag) {
        return String.format(HTML_LINK, getUrl(tag), tag.getName());
    }

    @Override
    public TagModel getModel(Tag tag) throws BusinessException {
        return new TagModel(tag.getName(), settingsService.isTagFavorite(tag),
                settingsService.isTagHidden(tag));
    }
}
