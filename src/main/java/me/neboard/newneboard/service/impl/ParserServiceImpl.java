package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.parser.ParserResult;
import me.neboard.newneboard.parser.TextParser;
import me.neboard.newneboard.parser.ParserLinkResolver;
import me.neboard.newneboard.service.ParserService;
import me.neboard.newneboard.user.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("parserService")
public class ParserServiceImpl implements ParserService {
    @Autowired
    private TextParser parser;

    @Autowired
    private SettingsService settingsService;

    @Override
    public String preparse(String rawText) throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.preparse(method, rawText);
    }

    @Override
    public ParserResult parse(String rawText, ParserLinkResolver resolver) throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.parse(method, rawText, resolver);
    }

    @Override
    public String getPanelText() throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.getPanelText(method);
    }

    @Override
    public List<String> getMethods() throws BusinessException {
        return parser.getMethods().stream()
                .map(methodData -> methodData.getName())
                .collect(Collectors.toList());
    }

    @Override
    public int getLineBreaks() throws BusinessException {
        String method = settingsService.getParseMethod();
        return parser.getMethods().stream()
                .filter(methodData -> method.equals(methodData.getName()))
                .findFirst()
                .map(methodData -> (int) methodData.getLineBreaks())
                .orElse(1);
    }

}
