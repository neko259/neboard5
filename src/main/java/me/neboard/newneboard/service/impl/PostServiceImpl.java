package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.*;
import me.neboard.newneboard.domain.manytomany.PostAttachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.PermissionException;
import me.neboard.newneboard.form.CreatableForm;
import me.neboard.newneboard.parser.ParserLinkResolver;
import me.neboard.newneboard.parser.ParserResult;
import me.neboard.newneboard.parser.PostParserLinkResolver;
import me.neboard.newneboard.repository.PostRepository;
import me.neboard.newneboard.rest.object.*;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.service.post.ThreadConfigService;
import me.neboard.newneboard.user.SettingsService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectDeletedException;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);

	private static final String LOG_THREAD = "Created thread %s with title %s and %d attachments";
	private static final String LOG_POST = "Created post %s in thread %s with %d attachments";

	private static final String HTML_LINK = "<a href='%s'>&gt;&gt;%d</a>";

	private static final String URL_THREAD = "/thread/";

	private static final String CACHE_POST_COUNT_BY_THREAD = "postCountByThread";
	private static final String CACHE_POST_REFS = "postRefs";
	private static final String CACHE_POST_HTML_LINK = "postHtmlLink";
	private static final String CACHE_POST_URL = "postUrl";

	private static final String DELIMITER_REPLY_REF = ",";

	@Autowired
	private ThreadService threadService;

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private TagService tagService;

	@Autowired
	private AttachmentService attachmentService;

	@Autowired
	private FormAttachmentService formAttachmentService;

	@Autowired
	private ViewService viewService;

	@Autowired
	private BanService banService;

	@Autowired
	private ParserService parserService;

	@Autowired
	private UtilService utilService;

	@Autowired
	private CacheService cacheService;

	@Autowired
	private TripcodeService tripcodeService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private ThreadConfigService threadConfigService;

	private ParserLinkResolver resolver;

	@PostConstruct
	private void initParserResolver() {
		resolver = new PostParserLinkResolver(this);
	}

	@Override
	public Post create(PostCreateDetails details) throws BusinessException {
		Ban ban = banService.findByAddress(details.getPosterAddress());
		if (ban != null) {
			throw new PermissionException("Banned for reason: " + ban.getReason());
		}

		boolean opening = details.isOpening();
		PostThread thread;
		if (opening) {
			Set<Tag> tags = tagService.getTagsFromNames(details.getTagNames());
			thread = new PostThread(tags, threadConfigService.getPostLimit());
			thread = threadService.save(thread);
		} else {
			thread = details.getThread();
			if (!thread.canPost()) {
				throw new BusinessException("Cannot post into archived thread");
			}
		}

		String preparsedRawText = parserService.preparse(details.getText());
		ParserResult parserResult = parserService.parse(preparsedRawText, resolver);
		List<Attachment> attachments = details.getAttachments();
		Post post = new Post(details.getTitle(), preparsedRawText, parserResult.getText(),
				new Date(), thread, opening, getPostAttachments(attachments),
				details.getPosterAddress(), details.getTripcode());

		post = save(post);
		connectReplies(post, parserResult.getReflinks());

		threadService.bump(thread, getCountByThread(thread), post.getPubTime());

		if (opening) {
			LOGGER.info(String.format(LOG_THREAD, post.toString(),
					getShortTitle(post), attachments.size()));
		} else {
			LOGGER.info(String.format(LOG_POST, post.toString(),
					post.getThread().getOpeningPost().toString(),
					attachments.size()));
		}

		return post;
	}

	private List<PostAttachment> getPostAttachments(List<Attachment> attachments) {
		List<PostAttachment> postAttachments = new ArrayList<>();
		int order = 0;

		// FIXME Currently only one post-attachment mapping can be stored for
		// a post and attachment
		Set<Long> processed = new HashSet<>();
		for (Attachment attachement : attachments) {
			if (!processed.contains(attachement.getId())) {
				postAttachments.add(new PostAttachment(attachement, order));
				processed.add(attachement.getId());
				order++;
			}
		}

		return postAttachments;
	}

	@Override
	public Post findById(long id) {
		return postRepository.findById(id).orElse(null);
	}

	@Override
	public double getPostSpeed() {
		LocalDate dateBefore = LocalDate.now().minusWeeks(1);

		return postRepository.countByPubTimeGreaterThan(java.sql.Date.valueOf(dateBefore)) / 7d;
	}

	@Override
	public int getCountByThread(PostThread thread) {
		return cacheService.get(CACHE_POST_COUNT_BY_THREAD, cacheService.getKey(thread.getId()),
				() -> postRepository.countByThread(thread), Integer.class);
	}

	@Override
	public <T> List<T> findByThreadAndIdIn(PostThread thread, List<Long> ids, Class<T> type) {
		return postRepository.findByThreadAndIdIn(thread, ids, type);
	}

	@Override
	public <T> List<T> findByThread(PostThread thread, Class<T> type) {
		return postRepository.findByThread(thread, type);
	}

	@Override
	public Page<Post> findByExample(Pageable pageable, Example<Post> example) {
		return postRepository.findAll(example, pageable);
	}

	@Override
	public void updateByAttachmentFilename(String filename) {
		List<Post> posts = postRepository.findByPostAttachmentsAttachmentFilename(filename);
		for (Post post : posts) {
			save(post);
		}
	}

	@Override
	public Page<Post> findByAttachmentFilename(Pageable pageable, String filename) {
		return postRepository.findByPostAttachmentsAttachmentFilename(pageable, filename);
	}

	@Override
	public Page<Post> search(Pageable pageable, String query) {
		Page<Post> page;
		if (StringUtils.isBlank(query)) {
			page = new PageImpl<>(Collections.emptyList());
		} else {
			page = postRepository.findByTitleContainingIgnoreCaseOrTextContainingIgnoreCase(pageable, query, query);
		}
		return page;
	}

	@Override
	public void delete(Post post) {
		PostThread thread = post.getThread();

		evictPostCaches(post, thread);
		cacheService.evict(CACHE_POST_URL, post.getId());
		cacheService.evict(CACHE_POST_HTML_LINK, post.getId());

		Collection<Post> referencedPosts = post.getReferencedPosts();

		postRepository.delete(post);

		for (Post refPost : referencedPosts) {
			cacheService.evict(CACHE_POST_REFS, refPost.getId());
		}

		// Return the thread from bumplimit if it was not reached before deletion
		if (thread.getStatus() == PostThread.Status.BUMPLIMIT && getCountByThread(thread) < thread.getPostLimit()) {
			thread.setStatus(PostThread.Status.ACTIVE);
			threadService.save(thread);
		}
	}

	private void evictPostCaches(Post post, PostThread thread) {
		cacheService.evict(CACHE_POST_COUNT_BY_THREAD, thread.getId());
		cacheService.evict(AttachmentService.CACHE_ATTACH_COUNT_BY_THREAD, thread.getId());
		cacheService.evict(CACHE_POST_REFS, post.getId());
	}

	@Override
	public String getUrl(Post post) {
		return cacheService.get(CACHE_POST_URL, cacheService.getKey(post.getId()),
				() -> post.isOpening() ? URL_THREAD + post.getId() : URL_THREAD + post.getThread().getOpeningPost().getId() + "#" + post.getId(), String.class);
	}

	public String getReplyRefs(Post post) {
		return cacheService.get(CACHE_POST_REFS, cacheService.getKey(post.getId()),
				() -> {
					String refs;
					Collection<Post> replies = post.getReplies();
					if (replies.isEmpty()) {
						refs = null;
					} else {
						refs = replies.stream()
								.map(reply -> getRef(reply).toString())
								.collect(Collectors.joining(DELIMITER_REPLY_REF));
					}
					return refs;
				}, String.class);
	}

	@Override
	@Cacheable(value = CACHE_POST_HTML_LINK, key = "#post.getId()")
	public String getHtmlLink(Post post) {
		String link = String.format(HTML_LINK, getUrl(post), post.getId());
		if (post.isOpening()) {
			link = "<b>" + link + "</b>";
		}
		return link;
	}

	@Override
	public List<Post> findTodayActiveThreads(Tag tag) {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime nowMinusDay = now.minusDays(1);
		Date dayDate = Date.from(nowMinusDay.atZone(ZoneId.systemDefault()).toInstant());

		List<Post> todayOpenings;

		if (tag == null) {
			todayOpenings = postRepository.findByOpeningAndThreadBumpTimeGreaterThanOrderByIdDesc(true, dayDate);
		} else {
			todayOpenings = postRepository.findByOpeningAndThreadTagsInAndThreadBumpTimeGreaterThanOrderByIdDesc(true, Collections.singletonList(tag), dayDate);
		}

		return todayOpenings;
	}

	@Override
	public Post updatePost(long postId, CreatableForm form) throws BusinessException {
		Post post = findById(postId);

		String text = form.getText();
		String preparsedRawText = parserService.preparse(text);

		post.setRawText(preparsedRawText);
		ParserResult parserResult = parserService.parse(preparsedRawText, resolver);
		post.setText(parserResult.getText());

		post.setTitle(form.getTitle());

		save(post);
		connectReplies(post, parserResult.getReflinks());

		String[] tagNames = form.getTagNames();
		if (tagNames != null) {
			Set<Tag> tags = tagService.getTagsFromNames(tagNames);

			PostThread thread = post.getThread();

			thread.getTags().clear();
			thread.getTags().addAll(tags);

			threadService.save(thread);
		}

		return post;
	}

	@Override
	public void purgeFromPost(Post post) {
		List<Post> posts = findByPosterAddress(post.getPosterAddress(), post.getId());
		purgePosts(posts, false);
	}

	@Override
	public void purgePosts(List<Post> posts, boolean ban) {
		for (Post post : posts) {
			try {
				if (ban) {
					banService.banUser(post.getPosterAddress(), "Post #" + post.getId());
				}
				if (post.isOpening()) {
					threadService.delete(post.getThread());
				} else {
					delete(post);
				}
			} catch (EntityNotFoundException | ObjectDeletedException e) {
				// Don't bother, this means we removed the post's thread earlier
			}
		}
	}

	@Override
	public long getCount() {
		return postRepository.count();
	}

	@Override
	public Collection<Post> getThreadLastPosts(PostThread thread) {
		Pageable lastPosts = utilService.getPageable(Optional.empty(),
				threadConfigService.getLastPostCount(), Sort.by(Sort.Order.desc("id")));
		Page<Post> page = postRepository.findByThreadAndOpening(lastPosts, thread, false);

		List<Post> result = new ArrayList<>(page.getContent());
		Collections.reverse(result);

		return result;
	}

	@Override
	public PostCreateDetails getCreateDetails(CreatableForm form, PostThread thread, String address) throws BusinessException {
		PostCreateDetails details = new PostCreateDetails();

		List<Attachment> attachments = formAttachmentService.createFromFormData(form.getProcessedAttachments());
		details.setAttachments(attachments);

		details.setTitle(form.getTitle().trim());
		details.setText(form.getText().trim());
		details.setTagNames(form.getTagNames());
		details.setTripcode(tripcodeService.encode(form.getTripcode()));

		details.setPosterAddress(address);
		details.setThread(thread);

		return details;
	}

	@Override
	public String getShortTitle(Post post) {
		String title = post.getTitle();
		if (StringUtils.isEmpty(title)) {
			title = Jsoup.parse(post.getText()).text();
			title = StringUtils.abbreviate(title, threadConfigService.getShortTitleLength());
		}
		return title;
	}

	@Override
	public PostModel getModel(Post post) {
		Post openingPost = null;
		if (post.isOpening()) {
			openingPost = post.getThread().getOpeningPost();
		}
		return getModel(post, openingPost);
	}

	public PostRef getRef(Post post) {
		return new PostRef(post.getId(), getUrl(post), post.isOpening());
	}

	@Override
	public PostModel getModel(Post post, Post openingPost) {
		PostModel model = new PostModel();

		PostThread thread = post.getThread();

		model.setOpening(post.isOpening());
		model.setUrl(getUrl(post));
		model.setTitle(post.getTitle());
		model.setText(post.getText());
		model.setId(post.getId());
		model.setTripcode(post.getTripcode());
		model.setPubTime(post.getPubTime());
		model.setUuid(post.getUuid());
		model.setThreadStatus(thread.getStatus().toString());

		if (settingsService.isAdmin()) {
			model.setPosterAddress(post.getPosterAddress());
		}

		String replyRefs = getReplyRefs(post);
		if (replyRefs != null) {
			model.setReplies(Arrays.stream(replyRefs.split(DELIMITER_REPLY_REF))
					.map(this::getRef)
					.collect(Collectors.toList()));
		}

		if (!post.isOpening()) {
			if (openingPost == null) {
				openingPost = thread.getOpeningPost();
			}
			model.setThread(openingPost.getId());
			model.setThreadTitle(getShortTitle(openingPost));
		} else {
			// TODO Cache tag names of a post
			model.setTags(thread.getTags()
					.stream()
					.map(Tag::getName)
					.collect(Collectors.toList()));
			model.setPostCount(getCountByThread(thread));
			model.setAttachmentCount(attachmentService.getCountByThread(thread));
		}

		// TODO Render attachments on the client
		List<PostAttachmentModel> attachmentModels = new ArrayList<>();
		for (PostAttachment postAttachment : post.getPostAttachments()) {
			Attachment attachment = postAttachment.getAttachment();
			AttachmentModel attachmentModel = null;
			try {
				attachmentModel = new AttachmentModel(attachment.getFilename(),
						attachment.getUrl(), viewService.getType(attachment), viewService.getData(attachment));
				PostAttachmentModel pam = new PostAttachmentModel(attachmentModel);
				pam.setOrder(postAttachment.getOrder());
				attachmentModels.add(pam);
			} catch (BusinessException e) {
				LOGGER.error("Error during getting attachment view: " + e.getMessage());
				// ignore this attachment for now
			}
		}
		model.setAttachments(attachmentModels);

		return model;
	}

	public PostRef getRef(String refStr) {
		String[] parts = refStr.split(PostRef.DELIMITER);
		return new PostRef(Long.parseLong(parts[0]), parts[1],
				Boolean.parseBoolean(parts[2]));
	}

	private void connectReplies(Post post, List<Long> reflinks) {
		reflinks.stream()
				.filter(postId -> !postId.equals(post.getId()))
				.forEach(postId -> connectReply(post, postId));
	}

	private void connectReply(Post reply, Long referencedPostId) {
		Post referencedPost = findById(referencedPostId);
		if (referencedPost != null) {
			referencedPost.getReplies().add(reply);
			save(referencedPost);
		}
	}

	private Post save(Post post) {
		evictPostCaches(post, post.getThread());

		post.setUuid(UUID.randomUUID().toString());
		return postRepository.save(post);
	}

	private List<Post> findByPosterAddress(String address, long firstPostId) {
		return postRepository.findByPosterAddressAndIdGreaterThanEqual(address, firstPostId);
	}

}
