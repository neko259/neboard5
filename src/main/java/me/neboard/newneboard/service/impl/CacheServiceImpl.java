package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.abstracts.Action;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class CacheServiceImpl implements CacheService {
	private static final String DELIMITER_KEY = ":";

	@Autowired
	private CacheManager cacheManager;

	@Override
	public void evict(String cacheName, Object key) {
		if (key != null) {
			Cache cache = cacheManager.getCache(cacheName);
			if (cache != null) {
				cache.evict(key);
			}
		}
	}

	/**
	 * Get from cache, if absent then compute and put to cache
	 */
	@Override
	public <T> T get(String cacheName, Object key, Action<T> action, Class<T> clazz) {
		T obj = get(cacheName, key, clazz);
		if (obj == null) {
			try {
				obj = action.execute();
			} catch (BusinessException e) {
				throw new RuntimeException(e);
			}
			if (obj != null) {
				put(cacheName, key, obj);
			}
		}
		return obj;
	}

	@Override
	public <T> T get(String cacheName, Object key, Class<T> clazz) {
		T result = null;
		Cache cache = cacheManager.getCache(cacheName);
		if (cache != null) {
			result = cache.get(key, clazz);
		}
		return result;
	}

	@Override
	public void put(String cacheName, Object key, Object value) {
		if (key != null) {
			Cache cache = cacheManager.getCache(cacheName);
			if (cache != null) {
				cache.put(key, value);
			}
		}
	}

	@Override
	public void clear(String cacheName) {
		Cache cache = cacheManager.getCache(cacheName);
		if (cache != null) {
			cache.clear();
		}
	}

	@Override
	public String getKey(Object... keys) {
		return Arrays.stream(keys).map(String::valueOf).collect(Collectors.joining(DELIMITER_KEY));
	}

}
