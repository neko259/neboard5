package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.service.TripcodeService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class TripcodeServiceImpl implements TripcodeService {
    private static final int COLOR_LENGTH = 6;

    @Override
    public String encode(String tripcodeSource) {
        return StringUtils.isNotBlank(tripcodeSource)
                ? DigestUtils.md5Hex(tripcodeSource).toUpperCase()
                : null;
    }

}
