package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class GatewayServiceImpl implements GatewayService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public <T> ResponseEntity<T> get(URI thirdPartyApi, Class<T> resultClass) {
        return restTemplate.exchange(thirdPartyApi,
                HttpMethod.GET, null,
                resultClass);
    }

    @Override
    public <T> ResponseEntity<T> get(URI thirdPartyApi, HttpHeaders headers, Class<T> resultClass) {
        return restTemplate.exchange(thirdPartyApi,
                HttpMethod.GET, new HttpEntity<>(headers),
                resultClass);
    }

}
