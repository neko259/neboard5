package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.attachment.viewer.AttachmentViewer;
import me.neboard.newneboard.attachment.viewer.FileViewer;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.ViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ViewServiceImpl implements ViewService {

    @Autowired
    private List<AttachmentViewer> viewers;

    @Autowired
    private FileViewer fileViewer;


    private AttachmentViewer getViewer(Attachment attachment) {
        AttachmentViewer result = null;

        for (AttachmentViewer viewer : viewers) {
            try {
                if (viewer.handles(attachment)) {
                    result = viewer;
                    break;
                }
            } catch (BusinessException e) {
                // Go to the next viewer, ideally missing viewer will catch
                // the files whose mimetype cannot be computed.
            }
        }

        if (result == null) {
            result = fileViewer;
        }

        return result;
    }

    @Override
    public Map<String, String> getData(Attachment attachment) throws BusinessException {
        AttachmentViewer viewer = getViewer(attachment);

        return viewer.getData(attachment);
    }

    @Override
    public String getType(Attachment attachment) {
        AttachmentViewer viewer = getViewer(attachment);

        return viewer.getType();
    }

}
