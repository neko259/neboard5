package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.repository.ThreadRepository;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.service.ThreadService;
import me.neboard.newneboard.service.post.ThreadConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("threadService")
@Transactional
@EnableScheduling
public class ThreadServiceImpl implements ThreadService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ThreadServiceImpl.class);

	@Autowired
	private ThreadRepository threadRepository;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private ThreadConfigService threadConfigService;

	@Override
	public Page<PostThread> findAll(Pageable pageable) {
		Page<PostThread> result;

		Set<Tag> hiddenTags = settingsService.getHiddenTags();

		if (hiddenTags.isEmpty()) {
			result = threadRepository.findAllByOrderByBumpTimeDesc(pageable);
		} else {
			result = threadRepository.findAllExcludingHiddenTags(
					hiddenTags, pageable);
		}
		return result;
	}

	@Override
	public PostThread save(PostThread thread) {
		return threadRepository.save(thread);
	}

	@Override
	public long getCount() {
		return threadRepository.count();
	}

	@Override
	public Page<PostThread> findByTag(Tag tag, Pageable pageable) {
		Page<PostThread> result;

		Set<Tag> hiddenTags = new HashSet<>(settingsService.getHiddenTags());
		hiddenTags.remove(tag);

		if (hiddenTags.isEmpty()) {
			result = threadRepository.findByTagsOrderByBumpTimeDesc(tag, pageable);
		} else {
			result = threadRepository.findByTagExcludingHiddenTags(tag,
					hiddenTags, pageable);
		}

		return result;
	}

	@Scheduled(cron = "0 0 0 * * ?")
	public void archiveThreads() {
		LocalDate oldestThreadDate = LocalDate.now().minusDays(threadConfigService.getThreadMaxAge());

		LOGGER.info("Started thread rotation");
		List<PostThread> threads = threadRepository.findByBumpTimeLessThan(java.sql.Date.valueOf(oldestThreadDate));
		for (PostThread thread : threads) {
			thread.setStatus(PostThread.Status.ARCHIVED);
			threadRepository.save(thread);

			LOGGER.info(String.format("Archived thread %s", thread));
		}
	}

	@Override
	public void delete(PostThread thread) {
		threadRepository.delete(thread);
	}

	@Override
	public PostThread findById(Long id) {
		return threadRepository.findById(id).orElse(null);
	}
	@Override
	public void bump(PostThread thread, int existingPostsCount, Date time) {
		if (PostThread.Status.ACTIVE.equals(thread.getStatus())) {
			thread.setBumpTime(time);

			if (existingPostsCount >= thread.getPostLimit()) {
				thread.setStatus(PostThread.Status.BUMPLIMIT);
			}

			save(thread);
		}
	}

}
