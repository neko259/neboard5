package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.repository.AttachmentRepository;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.CacheService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.web.ExternalImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {
    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private CacheService cacheService;

    @Override
    @Cacheable(value = CACHE_ATTACH_COUNT_BY_THREAD, key = "#thread.getId()")
    public int getCountByThread(PostThread thread) {
        return attachmentRepository.countByPostAttachmentsPostThread(thread);
    }

    @Override
    @Cacheable(value = "missingFiles")
    public Set<String> getMissingFiles() {
        return attachmentRepository.findByUrlNull().stream()
                .map(Attachment::getFilename)
                .filter(fileService::fileExists)
                .collect(Collectors.toSet());
    }

    @Override
    public List<String> getFileUrlList() {
        return attachmentRepository.findByUrlNull().stream()
                .map(this::getUrl)
                .collect(Collectors.toList());
    }

    @Override
    public long getCount() {
        return attachmentRepository.count();
    }

    @Override
    public Attachment findByFilename(String filename) {
        return attachmentRepository.findByFilename(filename);
    }

    @Override
    public Attachment save(Attachment attachment) {
        return attachmentRepository.save(attachment);
    }

    @Override
    public String getUrl(Attachment attachment) {
        return attachment.isFileAttachment() ?
                ExternalImageController.URL_FILES + attachment.getFilename() :
                attachment.getUrl();
    }

    @Override
    public void markAsDownloaded(String url, String filename) {
        Attachment attachment = attachmentRepository.findByUrl(url);
        if (attachment != null) {
            attachment.setFilename(filename);
            save(attachment);
        }
    }

//    /**
//     * Remove attachments that are no longer referenced by any post
//     */
//    @Scheduled(cron = "0 0 0 * * ?")
//    public void removeUnused() throws BusinessException {
//        /*
//         * TODO Clean up files without attachments
//         * This can be split into 2 operations:
//         * 1. Remove unused attachments but don't touch the files
//         * 2. Go through files and remove the ones not linked to
//         * any attachment
//         */
//        LOGGER.info("Removing unused attachments");
//        List<Attachment> attachments = attachmentRepository.findWithoutPosts();
//        for (Attachment attachment : attachments) {
//            if (!attachment.isUrlAttachment()) {
//                fileService.delete(attachment.getFilename());
//            }
//
//            attachmentRepository.delete(attachment);
//            LOGGER.info(String.format("Removed attachment %s", attachment));
//        }
//    }

    @Override
    public Attachment findByUrl(String url) {
        return attachmentRepository.findByUrl(url);
    }

    @Override
    public List<Attachment> findByThread(PostThread thread) {
        return attachmentRepository.findDistinctByPostAttachmentsPostThread(thread);
    }
}
