package me.neboard.newneboard.service.impl;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.exception.NotFoundException;
import me.neboard.newneboard.service.ExternalService;
import me.neboard.newneboard.service.FileService;
import me.neboard.newneboard.service.ImageService;
import me.neboard.newneboard.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class ImageServiceImpl implements ImageService {
    private static final String PREFIX_THUMB = "thumb_";
    private static final String DIR_STATIC = "static";

    private static final String EXT_URL_DOMAIN_IMAGE = "tag/domain:%s";
    private static final String EXT_URL_FILE_IMAGE = "tag/%s";

    @Value("${image.thumb.width}")
    private int thumbWidth;

    @Value("${image.thumb.height}")
    private int thumbHeight;

    @Autowired
    private FileService fileService;

    @Autowired
    private UtilService utilService;

    @Autowired
    private ExternalService externalService;

    @Override
    public boolean canCreateThumb(String mimetype) {
        return utilService.isImage(mimetype);
    }

    @Override
    public FileMetadata createThumb(String filename) throws BusinessException {
        Map<String, Object> options = new HashMap<>();
        options.put("source", filename);
        options.put("type", "thumb");
        options.put("thumbPrefix", PREFIX_THUMB);
        options.put("thumbWidth", thumbWidth);
        options.put("thumbHeight", thumbHeight);
        return fileService.create(options);
    }

    @Override
    public String getThumbName(String originalImageName) {
        // TODO Get from file service. Thumb name should be generated just
        // like the rest of files
        return PREFIX_THUMB + originalImageName;
    }

    @Override
    @Cacheable(key = "#filename", value = "imageSizeClasspath")
    public Dimensions getStaticImageSizeFromClasspath(String filename) throws BusinessException {
        BufferedImage bimg;
        try (InputStream is = new ClassPathResource(new File(DIR_STATIC, filename).getPath()).getInputStream()) {
            bimg = ImageIO.read(is);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
        return getDimensions(bimg);
    }

    @Override
    public Dimensions getDimensions(BufferedImage bimg) {
        int width = bimg.getWidth();
        int height = bimg.getHeight();

        return new Dimensions(width, height);
    }

    @Override
    @Cacheable(value = ExternalService.CACHE_DOMAIN_IMAGE, key = "#domain")
    public ExternalImageData getDomainImage(String domain) throws BusinessException {
        try {
            return new ExternalImageData(externalService.requestGet(
                    externalService.getFileServiceUrl(String.format(EXT_URL_DOMAIN_IMAGE, domain)),
                    null,
                    String[].class));
        } catch (NotFoundException e) {
            return null;
        }
    }

    @Override
    @Cacheable(value = ExternalService.CACHE_FILE_IMAGE, key = "#mimetype")
    public ExternalImageData getFileImage(String mimetype) throws BusinessException {
        try {
            return new ExternalImageData(externalService.requestGet(
                    externalService.getFileServiceUrl(String.format(EXT_URL_FILE_IMAGE, mimetype)),
                    null,
                    String[].class));
        } catch (NotFoundException e) {
            return null;
        }
    }

}
