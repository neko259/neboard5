package me.neboard.newneboard.service;

public interface JsonService {
    <T> T fromJson(String json, Class<T> type);
    String toJson(Object obj);
}
