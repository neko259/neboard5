package me.neboard.newneboard.service;

import me.neboard.newneboard.exception.BusinessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.util.Map;

public interface ExternalService {
    String CACHE_DOMAIN_IMAGE = "domainImage";
    String CACHE_FILE_IMAGE = "fileImage";

    String getParserServiceUrl();

    String getFileServiceUrl();

    String getFileServiceUrl(String relativeUrl);

    <T> T requestPost(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException;
    <T> T requestGet(String url, Map<String, Object> data, Class<T> resultClass) throws BusinessException;
    <T> T request(String url, HttpMethod method, Map<String, Object> data, HttpHeaders headers, Class<T> resultClass) throws BusinessException;

    String rawRequest(String url, HttpMethod method, Object body, HttpHeaders headers) throws BusinessException;

    String concatUrl(String parentUrl, String childUrl);
}
