package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.rest.object.ThreadPreviewModel;
import me.neboard.newneboard.rest.object.ThreadRef;


public interface ThreadPostService extends ApiService<PostThread, ThreadPreviewModel>{
    int getSkippedRepliesCount(PostThread thread);
    ThreadPreviewModel getModel(PostThread thread) throws BusinessException;
    ThreadRef getRef(Post openingPost);
    ThreadRef getRef(PostThread thread);
}
