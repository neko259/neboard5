package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Post;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.form.CreatableForm;
import me.neboard.newneboard.rest.object.PostModel;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface PostService extends SearchService<Post>, LinkAwareService<Post>,
        DeleteAwareService<Post>, ApiService<Post, PostModel> {
    Post create(PostCreateDetails details) throws BusinessException;

    double getPostSpeed();

    int getCountByThread(PostThread thread);

    Post findById(long id);
    <T> List<T> findByThreadAndIdIn(PostThread thread, List<Long> ids, Class<T> type);
    <T> List<T> findByThread(PostThread thread, Class<T> type);
    Page<Post> findByAttachmentFilename(Pageable pageable, String filename);
    Page<Post> findByExample(Pageable pageable, Example<Post> example);
    void updateByAttachmentFilename(String filename);

    List<Post> findTodayActiveThreads(Tag tag);

    Post updatePost(long postId, CreatableForm form) throws BusinessException;

    void purgeFromPost(Post post);

    void purgePosts(List<Post> posts, boolean ban);

    long getCount();

    Collection<Post> getThreadLastPosts(PostThread thread);

    PostCreateDetails getCreateDetails(CreatableForm form, PostThread thread, String address) throws BusinessException;

    String getShortTitle(Post post);

    PostModel getModel(Post post, Post openingPost);

}
