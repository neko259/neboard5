package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.PersistentEntity;
import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.user.SettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UtilService {
    @Value("${media.types.image}")
    private Set<String> imageMimetypes;

    private static final int DEFAULT_PAGE = 0;

    private static final String DELIMITER_HEADER = ",";

    private static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";

    private static final String HEADER_CACHE_CONTROL = "cache-control";
    private static final String CACHE_NO_CACHE = "no-cache, no-store, must-revalidate";

    @Value("${site.name}")
    private String siteName;

    @Value("${site.version}")
    private String siteVersion;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private TagService tagService;

    public String getSiteName() {
        return siteName;
    }

    public String getSiteVersion() {
        return siteVersion;
    }

    /**
     * Gets pageable object required for paged requests and output of some pages.
     */
    public Pageable getPageable(Optional<Integer> page, int perPage) {
        return PageRequest.of(page.orElse(DEFAULT_PAGE), perPage);
    }

    public Pageable getPageable(Optional<Integer> page, int perPage, Sort sort) {
        return PageRequest.of(page.orElse(DEFAULT_PAGE), perPage, sort);
    }

    /**
     * Gets unpaged object that is used to get all results.
     */
    public Pageable getPageable() {
        return Pageable.unpaged();
    }

    public boolean isImage(String mimetype) {
        return imageMimetypes.contains(mimetype);
    }

    public String getMessage(String key, Object... arguments) {
        try {
            return messageSource.getMessage(key, arguments, LocaleContextHolder.getLocale());
        } catch (NoSuchMessageException e) {
            return messageSource.getMessage(key, arguments, Locale.getDefault());
        }
    }

    public String getIpFromRequest(HttpServletRequest request) {
        String address;

        String xHeader = request.getHeader(HEADER_X_FORWARDED_FOR);
        if (xHeader == null) {
            address = request.getRemoteAddr();
        } else {
            address = xHeader.split(DELIMITER_HEADER)[0];
        }

        return address;
    }

    public Map<String, String> getMessages(Set<String> keys) {
        return keys.stream().collect(Collectors.toMap(key -> key, this::getMessage));
    }

    public void noCacheResponse(HttpServletResponse response) {
        response.addHeader(HEADER_CACHE_CONTROL, CACHE_NO_CACHE);
    }

    public String getUrlList(Collection<Tag> objects) {
        return getUrlList(objects, false);
    }

    public String getUrlList(Collection<Tag> objects, boolean trailingDelimiter) {
        String result = objects.stream()
                .map(tagService::getHtmlLink)
                .collect(Collectors.joining(PersistentEntity.DELIMITER));
        if (trailingDelimiter && !result.isEmpty()) {
            result += PersistentEntity.DELIMITER;
        }
        return result;
    }

}
