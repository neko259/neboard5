package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.domain.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface ThreadService extends DeleteAwareService<PostThread> {
    Page<PostThread> findAll(Pageable pageable);
    Page<PostThread> findByTag(Tag tag, Pageable pageable);
    PostThread findById(Long id);

    PostThread save(PostThread thread);

    long getCount();

    void bump(PostThread thread, int existingPostCount, Date time);

}
