package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.rest.object.TagModel;

import java.util.List;
import java.util.Set;

public interface TagService extends SearchService<Tag>, LinkAwareService<Tag>,
        ApiService<Tag, TagModel>{
    Tag findByName(String name);
    Tag getOrCreate(String name);
    List<Tag> findStartingWith(String name);
    Set<Tag> getTagsFromNames(String[] tagNames);
    boolean isValidName(String tagName);
}
