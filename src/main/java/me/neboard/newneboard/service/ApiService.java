package me.neboard.newneboard.service;

import me.neboard.newneboard.exception.BusinessException;

public interface ApiService<T, K> {
    K getModel(T entity) throws BusinessException;
}
