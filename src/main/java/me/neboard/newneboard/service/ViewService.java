package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

import java.util.Map;

public interface ViewService {
    Map<String, String> getData(Attachment attachment) throws BusinessException;
    String getType(Attachment attachment);
}
