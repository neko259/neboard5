package me.neboard.newneboard.service;

import me.neboard.newneboard.attachment.creation.AttachmentCreator;
import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.exception.BusinessException;

import java.util.List;

public interface FormAttachmentService {
    <T extends FormAttachment> AttachmentCreator<T> getCreator(T source);
    FormAttachment getAttachment(Object source);
    List<Attachment> createFromFormData(List<FormAttachment> formAttachments) throws BusinessException;
}
