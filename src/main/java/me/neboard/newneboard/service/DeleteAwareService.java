package me.neboard.newneboard.service;

import me.neboard.newneboard.domain.PersistentEntity;

public interface DeleteAwareService<T extends PersistentEntity> {
    void delete(T entity);
}
