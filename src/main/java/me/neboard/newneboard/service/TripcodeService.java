package me.neboard.newneboard.service;

public interface TripcodeService {
    String encode(String tripcodeSource);
}
