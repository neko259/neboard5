package me.neboard.newneboard.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.net.URI;

public interface GatewayService {
    <T> ResponseEntity<T> get(URI thirdPartyApi, Class<T> resultClass);
    <T> ResponseEntity<T> get(URI thirdPartyApi, HttpHeaders headers, Class<T> resultClass);
}
