package me.neboard.newneboard.service.post;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ThreadConfigServiceImpl implements ThreadConfigService {
    @Value("${thread.post.limit}")
    private int postLimit;

    @Value("${short.title.length}")
    private int shortTitleLength;

    @Value("${all.last.posts}")
    private Integer lastPostsCount;

    @Value("${thread.max.age}")
    private int threadMaxAge;

    @Override
    public int getPostLimit() {
        return postLimit;
    }

    @Override
    public int getLastPostCount() {
        return lastPostsCount;
    }

    @Override
    public int getShortTitleLength() {
        return shortTitleLength;
    }

    @Override
    public int getThreadMaxAge() {
        return threadMaxAge;
    }
}
