package me.neboard.newneboard.service.post;

public interface ThreadConfigService {
    int getPostLimit();
    int getLastPostCount();
    int getShortTitleLength();
    int getThreadMaxAge();
}
