package me.neboard.newneboard.service;

import me.neboard.newneboard.abstracts.Dimensions;
import me.neboard.newneboard.abstracts.ExternalImageData;
import me.neboard.newneboard.abstracts.FileMetadata;
import me.neboard.newneboard.exception.BusinessException;

import java.awt.image.BufferedImage;

public interface ImageService {
    boolean canCreateThumb(String mimetype);
    FileMetadata createThumb(String filename) throws BusinessException;
    String getThumbName(String originalImageName);
    Dimensions getStaticImageSizeFromClasspath(String filename) throws BusinessException;
    Dimensions getDimensions(BufferedImage bimg);

    ExternalImageData getDomainImage(String domain) throws BusinessException;
    ExternalImageData getFileImage(String mimetype) throws BusinessException;
}
