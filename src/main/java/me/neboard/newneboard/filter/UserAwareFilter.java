package me.neboard.newneboard.filter;

import jakarta.servlet.*;
import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.user.LocalContextService;
import me.neboard.newneboard.user.SettingsService;
import me.neboard.newneboard.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class UserAwareFilter implements Filter {
    private static final String ATTR_USER = "user";

    private static final String ATTR_ADMIN = "admin";
    private static final String ATTR_THEME = "theme";

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private UserService userService;

    @Autowired
    private LocalContextService localContextService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        User user = userService.getUser();
        localContextService.createContext(user);

        request.setAttribute(ATTR_USER, user);

        request.setAttribute(ATTR_ADMIN, settingsService.isAdmin());
        request.setAttribute(ATTR_THEME, settingsService.getTheme());

        chain.doFilter(request, response);
    }

}
