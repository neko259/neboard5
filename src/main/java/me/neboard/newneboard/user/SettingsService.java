package me.neboard.newneboard.user;

import me.neboard.newneboard.domain.Tag;
import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.exception.PermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class SettingsService {
    private static final String DELIMITER_THEME = ":";

    @Value("${themes}")
    private List<String> themes;

    @Autowired
    private LocalContextService localContextService;

    @Autowired
    private UserService userService;

    public Map<String, String> getThemes() {
        return themes.stream().map(theme -> theme.split(DELIMITER_THEME)).collect(Collectors.toMap(theme -> theme[0], theme -> theme[1]));
    }

    public String getTheme() {
        return getContext().getTheme();
    }

    public void saveTheme(String theme) {
        User user = userService.getUser();
        user.setTheme(theme);
        userService.save(user);
    }

    public Set<Tag> getFavoriteTags() {
        return userService.getUser().getFavoriteTags();
    }

    public Set<Tag> getHiddenTags() {
        return userService.getUser().getHiddenTags();
    }

    public void addFavoriteTag(Tag tag) {
        User user = userService.getUser();

        user.getFavoriteTags().add(tag);
        userService.save(user);
    }

    public void removeFavoriteTag(Tag tag) {
        User user = userService.getUser();

        user.getFavoriteTags().remove(tag);
        userService.save(user);
    }

    public void addHiddenTag(Tag tag) {
        User user = userService.getUser();

        user.getHiddenTags().add(tag);
        userService.save(user);
    }

    public void removeHiddenTag(Tag tag) {
        User user = userService.getUser();

        user.getHiddenTags().remove(tag);
        userService.save(user);
    }

    public boolean isTagFavorite(Tag tag) {
        Set<Tag> favoriteTags = userService.getUser().getFavoriteTags();
        if (favoriteTags == null) {
            return false;
        } else {
            return favoriteTags.contains(tag);
        }
    }

    public boolean isTagHidden(Tag tag) {
        Set<Tag> tags = userService.getUser().getHiddenTags();
        if (tags == null) {
            return false;
        } else {
            return tags.contains(tag);
        }
    }


    public boolean isAdmin() {
        return getContext().isAdmin();
    }

    public void validatePermission() throws PermissionException {
        if (!isAdmin()) {
            throw new PermissionException("Only admin can do that");
        }
    }

    public void saveParseMethod(String parseMethod) {
        User user = userService.getUser();
        user.setParseMethod(parseMethod);
        userService.save(user);
    }

    public String getParseMethod() {
        return getContext().getParseMethod();
    }

    private UserContext getContext() {
        return localContextService.getContext();
    }

}
