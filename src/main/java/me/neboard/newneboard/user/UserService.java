package me.neboard.newneboard.user;

import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.exception.BusinessException;

public interface UserService {
    User getUser();
    User findByUuid(String uuid);
    User save(User user);

    void login(String uuid) throws BusinessException;
}
