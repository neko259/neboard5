package me.neboard.newneboard.user;

import me.neboard.newneboard.domain.User;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

@Service
@EnableScheduling
public class UserServiceImpl implements UserService {
    private static final String ATTR_USER_ID = "userUuid";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${users.timeout}")
    private Duration timeout;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private LocalContextService localContextService;

    @Override
    public User getUser() {
        String uuid = getCookie(ATTR_USER_ID);

        User user = null;
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        } else {
            user = findByUuid(uuid);
        }

        if (user == null) {
            user = save(new User(uuid));
            LOGGER.debug(String.format("Created user %s", user));
            setCookie(ATTR_USER_ID, uuid);
        }

        return user;
    }

    @Override
    public User findByUuid(String uuid) {
        return userRepository.findByUuid(uuid);
    }

    @Override
    public User save(User user) {
        user.setLastOperatedTime(new Date());
        return userRepository.save(user);
    }

    @Override
    public void login(String uuid) throws BusinessException {
        // Login only when not the same user we are using now
        if (!uuid.equals(getCookie(ATTR_USER_ID))) {
            User user = findByUuid(uuid);
            if (user != null) {
                setCookie(ATTR_USER_ID, uuid);
                user = save(user);
                localContextService.createContext(user);
                LOGGER.info("Login performed for user " + user);
            } else {
                throw new BusinessException("No such user");
            }
        }
    }

    @Scheduled(cron = "0 0 * * * ?")
    @Transactional
    public void removeInactiveUsers() {
        LocalDate oldestUserDate = LocalDate.now().minusDays(timeout.toDays());

        LOGGER.debug("Started user cleanup");
        long deletedCount = userRepository.deleteByLastOperatedTimeLessThan(java.sql.Date.valueOf(oldestUserDate));

        if (deletedCount > 0) {
            LOGGER.info(String.format("Deleted %s users", deletedCount));
        }
    }

    private String getCookie(String name) {
        Cookie[] cookies = request.getCookies();
        String value = null;

        if (cookies != null) {
            value = Arrays.stream(cookies)
                    .filter(cookie -> name.equals(cookie.getName()))
                    .findFirst()
                    .map(Cookie::getValue)
                    .orElse(null);
        }

        return value;
    }

    private void setCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

}
