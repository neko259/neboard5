package me.neboard.newneboard.user;

import me.neboard.newneboard.domain.User;

public interface LocalContextService {
    UserContext getContext();
    UserContext createContext(User user);
}
