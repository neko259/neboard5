package me.neboard.newneboard.user;

import lombok.Data;

@Data
public class UserContext {
    boolean admin;
    String theme;
    String parseMethod;
}
