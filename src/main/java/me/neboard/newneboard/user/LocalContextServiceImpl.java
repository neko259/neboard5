package me.neboard.newneboard.user;

import me.neboard.newneboard.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class LocalContextServiceImpl implements LocalContextService {
    private ThreadLocal<UserContext> userContext = new ThreadLocal<>();

    @Value("${admin.uuid}")
    private List<String> adminUuids;

    @Value("${text.parser.method}")
    private String defaultParseMethod;


    private static final String DEFAULT_THEME = "md";

    public UserContext getContext() {
        return userContext.get();
    }

    @Override
    public UserContext createContext(User user) {
        UserContext context = newContext(user);
        setContext(context);
        return context;
    }

    private void setContext(UserContext context) {
        userContext.set(context);
    }

    private UserContext newContext(User user) {
        UserContext context = new UserContext();
        context.setAdmin(isAdmin(user));
        context.setTheme(getTheme(user));
        context.setParseMethod(Objects.requireNonNullElse(user.getParseMethod(),
                defaultParseMethod));
        return context;
    }

    private String getTheme(User user) {
        return Objects.requireNonNullElse(user.getTheme(), DEFAULT_THEME);
    }

    public boolean isAdmin(User user) {
        return adminUuids.contains(user.getUuid());
    }

}
