package me.neboard.newneboard.amqp;

import java.io.Serializable;
import java.util.Map;

public class Message implements Serializable {
    private String type;
    private String status;
    private Map<String, Object> data;

    public Message(String type, String status, Map<String, Object> data) {
        this.type = type;
        this.status = status;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
