package me.neboard.newneboard.amqp.handlers;

import me.neboard.newneboard.amqp.Message;
import me.neboard.newneboard.service.AttachmentService;
import me.neboard.newneboard.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DownloadHandler implements MessageHandler {
    private static final String TYPE_DOWNLOAD = "download";

    private static final Logger log = LoggerFactory.getLogger(DownloadHandler.class);

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private PostService postService;

    @Override
    public boolean handles(Message message) {
        return TYPE_DOWNLOAD.equals(message.getType());
    }

    @Override
    public void handle(Message message) {
        Map<String, Object> data = message.getData();
        String url = (String) data.get("url");
        if ("COMPLETED".equals(message.getStatus())) {
            String filename = (String) data.get("filename");
            attachmentService.markAsDownloaded(url,
                    filename);
            log.info(String.format("Saving %s to %s", url, filename));

            // Update the filenames so the posts will have updated uuids and
            // refresh in browser
            postService.updateByAttachmentFilename(filename);
        } else {
            log.warn(String.format("URL %s was not downloaded", url));
        }
    }
}
