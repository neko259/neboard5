package me.neboard.newneboard.amqp.handlers;

import me.neboard.newneboard.amqp.Message;
import me.neboard.newneboard.exception.BusinessException;

public interface MessageHandler {
    boolean handles(Message message);
    void handle(Message message) throws BusinessException;
}
