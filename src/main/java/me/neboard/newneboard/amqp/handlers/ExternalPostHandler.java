package me.neboard.newneboard.amqp.handlers;

import me.neboard.newneboard.amqp.Message;
import me.neboard.newneboard.attachment.creation.attachment.FormAttachment;
import me.neboard.newneboard.domain.Attachment;
import me.neboard.newneboard.domain.PostThread;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.*;
import me.neboard.newneboard.service.impl.PostCreateDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ExternalPostHandler implements MessageHandler {
    private static final String TYPE_EXTERNAL_POST = "external-post";

    private static final Logger logger = LoggerFactory.getLogger(ExternalPostHandler.class);

    @Autowired
    private PostService postService;

    @Autowired
    private FormAttachmentService formAttachmentService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private TripcodeService tripcodeService;

    @Override
    public boolean handles(Message message) {
        return TYPE_EXTERNAL_POST.equals(message.getType());
    }

    @Override
    public void handle(Message message) throws BusinessException {
        Map<String, Object> data = message.getData();

        PostThread thread = postService.findById((long) data.get("thread")).getThread();

        List<FormAttachment> urls = new ArrayList<>();
        List<String> urlList = (List<String>) data.get("urls");
        if (urlList != null) {
            urls = urlList.stream()
                    .map(formAttachmentService::getAttachment)
                    .collect(Collectors.toList());
        }

        List<Attachment> attachments = formAttachmentService.createFromFormData(urls);

        PostCreateDetails details = new PostCreateDetails(
                (String) data.get("title"),
                (String) data.get("text"),
                attachments,
                thread,
                null,
                tripcodeService.encode((String) data.get("tripcode")));
        postService.create(details);
    }
}
