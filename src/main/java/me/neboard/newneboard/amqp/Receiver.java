package me.neboard.newneboard.amqp;

import com.google.gson.Gson;
import me.neboard.newneboard.amqp.handlers.MessageHandler;
import me.neboard.newneboard.exception.BusinessException;
import me.neboard.newneboard.service.AttachmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@Component
public class Receiver {
    private static final Logger logger = LoggerFactory.getLogger(Receiver.class);

    private Gson gson = new Gson();
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private List<MessageHandler> handlers;

    public void receiveMessage(String message) {
        Message msg = gson.fromJson(message, Message.class);
        latch.countDown();

        for (MessageHandler handler : handlers) {
            if (handler.handles(msg)) {
                try {
                    handler.handle(msg);
                } catch (BusinessException e) {
                   logger.error("Error during processing AMQP message", e);
                }
            }
        }
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}