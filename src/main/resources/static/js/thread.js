/*
 @licstart  The following is the entire license notice for the
 JavaScript code in this page.


 Copyright (C) 2013  neko259

 The JavaScript code in this page is free software: you can
 redistribute it and/or modify it under the terms of the GNU
 General Public License (GNU GPL) as published by the Free Software
 Foundation, either version 3 of the License, or (at your option)
 any later version.  The code is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

 As additional permission under GNU GPL version 3 section 7, you
 may distribute non-source (e.g., minimized or compacted) forms of
 that code without the copy of the GNU GPL normally required by
 section 4, provided you include this license notice and a URL
 through which recipients can access the Corresponding Source.

 @licend  The above is the entire license notice
 for the JavaScript code in this page.
 */

var REPLY_TO_MSG = '.reply-to-message';
var REPLY_TO_MSG_ID = '#reply-to-message-id';

var $html = $("html, body");

function moveCaretToEnd(el) {
    var newPos = el.val().length;
    el[0].setSelectionRange(newPos, newPos);
}


function showFormAfter(blockToInsertAfter) {
    var form = getForm();
    form.insertAfter(blockToInsertAfter);

    form.show();
    $(REPLY_TO_MSG_ID).text(blockToInsertAfter.attr('id'));
    $(REPLY_TO_MSG).show();
}

function getLineBreak(form) {
    var count = parseInt(form.find('form').attr('data-line-breaks'));

    var s = '';
    while (count--) {
        s += '\n';
    }
    return s;
}

function addQuickReply(postId) {
    var blockToInsert = null;
    var postForm = $('.post-form');
    var textAreaJq = getPostTextarea(postForm);
    var postLinkRaw = '>>' + postId;
    var textToAdd = '';

    var lineBreak = getLineBreak(postForm);

    if (postId != null) {
        var post = $('#' + postId);

        // If this is not OP, add reflink to the post. If there already is
        // the same reflink, don't add it again.
        var postText = textAreaJq.val();
        if (!post.is(':first-child') && postText.indexOf(postLinkRaw) < 0) {
            // Insert line break if none is present.
            var postLastChar = postText.slice(-1);
            if (postLastChar !== '' && postLastChar !== '\n' && postLastChar !== '\r') {
                textToAdd += lineBreak;
            }
            textToAdd += postLinkRaw + lineBreak;
        }

        textAreaJq.val(textAreaJq.val()+ textToAdd);
        blockToInsert = post;
    } else {
        blockToInsert = $('.thread');
    }
    showFormAfter(blockToInsert);

    textAreaJq.focus();

    moveCaretToEnd(textAreaJq);
}

function addQuickQuote() {
    var $quoteButton = $("#quote-button");

    $quoteButton.hide();

    var postForm = $('.post-form');
    var textAreaJq = getPostTextarea(postForm);
    var quotePanelButton = postForm.find('.mark_btn:has(.quote)');

    var postId = $quoteButton.attr('data-post-id');
    if (postId != null) {
        addQuickReply(postId);
    }

    var textToAdd = '';
    var selection = window.getSelection().toString();
    if (selection.length == 0) {
        selection = $quoteButton.attr('data-text');
    }
    if (selection.length > 0 && quotePanelButton.length > 0) {
        textToAdd += quotePanelButton.attr('data-tag-left') + selection
            + quotePanelButton.attr('data-tag-right')
            + getLineBreak(postForm);
    }

    textAreaJq.val(textAreaJq.val() + textToAdd);

    textAreaJq.focus();

    moveCaretToEnd(textAreaJq);
}

function scrollToBottom() {
    $html.animate({scrollTop: $html.height()}, "fast");
}

function showQuoteButton() {
    var sel = window.getSelection();
    var selection = null;
    if (sel.rangeCount > 0) {
        var selection = window.getSelection().getRangeAt(0).getBoundingClientRect();
    }
    var $quoteButton = $("#quote-button");
    if (selection != null && selection.width > 0) {
        $quoteButton.css({top: selection.top + $(window).scrollTop() - parseInt($quoteButton.css("height"), 10) - 3, left: selection.left});
        $quoteButton.show();

        var text = window.getSelection().toString();
        $quoteButton.attr('data-text', text);

        var rect = window.getSelection().getRangeAt(0).getBoundingClientRect();
        var element = $(document.elementFromPoint(rect.x, rect.y));
        var postId = null;
        if (element.hasClass('post')) {
            postId = element.attr('id');
        } else {
            var postParent = element.parents('.post');
            if (postParent.length > 0) {
                postId = postParent.attr('id');
            }
        }
        $quoteButton.attr('data-post-id', postId);
    } else {
        $quoteButton.hide();
    }
}

function initThread() {
    $('body').on('mouseup', function() {
        showQuoteButton();
    });
}
