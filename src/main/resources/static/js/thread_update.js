/*
 @licstart  The following is the entire license notice for the
 JavaScript code in this page.


 Copyright (C) 2013-2014  neko259

 The JavaScript code in this page is free software: you can
 redistribute it and/or modify it under the terms of the GNU
 General Public License (GNU GPL) as published by the Free Software
 Foundation, either version 3 of the License, or (at your option)
 any later version.  The code is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

 As additional permission under GNU GPL version 3 section 7, you
 may distribute non-source (e.g., minimized or compacted) forms of
 that code without the copy of the GNU GPL normally required by
 section 4, provided you include this license notice and a URL
 through which recipients can access the Corresponding Source.

 @licend  The above is the entire license notice
 for the JavaScript code in this page.
 */

var CLASS_POST = '.post';
var URL_DIFF = '/api/post/diff/';
var REQUEST_METHOD_DIFF = 'POST';
var DATA_TYPE_DIFF = 'json';

var THREAD_STATUS_BUMPLIMIT = 'BUMPLIMIT';
var THREAD_STATUS_ACTIVE = 'ACTIVE';
var THREAD_STATUS_ARCHIVE = 'ARCHIVE';

var JS_AUTOUPDATE_PERIOD = parseInt($('body').data('update-timeout'));
var BLINK_SPEED = 300;

var ALLOWED_FOR_PARTIAL_UPDATE = [
    'refmap',
    'post-info'
];

var ATTR_CLASS = 'class';
var ATTR_UUID = 'data-uuid';

var unreadPosts = 0;
var documentOriginalTitle = '';
var timeoutState;

// Thread ID does not change, can be stored one time
var threadId = $('div[data-thread-id]').attr('data-thread-id');

/**
 * Get diff of the posts from the current thread timestamp.
 * This is required if the browser was closed and some post updates were
 * missed.
 */
function getThreadDiff() {
    var uids = Array();
    $('.thread > .post').each(function() {
        uids.push($(this).attr(ATTR_UUID))
    });

    var data = {
        uuids: uids,
        threadId: threadId
    };


    $.ajax({
        type: REQUEST_METHOD_DIFF,
        url: URL_DIFF,
        data: JSON.stringify(data),
        contentType: CONTENT_TYPE_JSON,
        success: function(data) {
            var updatedPosts = data.updated;
            var deletedPosts = data.deleted;

            $.each(updatedPosts, function(index, postModel) {
                var post = render(postModel, ["replyButton"], "post");

                addOrUpdatePost(post);
            });

            $.each(deletedPosts, function(index, postUuid) {
                $('.thread > .post[data-uuid=' + postUuid + ']').remove();;
            });

            updateBumplimitProgress(data.postLimit);

            if (updatedPosts.length > 0 || deletedPosts.length > 0) {
                showNewPostsTitle(updatedPosts.length - deletedPosts.length);
            }

            processThreadStatusUpdate(data.threadStatus);
        },
        complete: function() {
            completeThreadUpdate();
            scheduleThreadUpdate();
        },
        dataType: DATA_TYPE_DIFF
    })
}

function processThreadStatusUpdate(status) {
    if (status == THREAD_STATUS_BUMPLIMIT) {
        var threadBlock = $('div.thread').children('.post:not(.dead_post)').each(function() {
            $(this).addClass('dead_post');
        });
    } else if (status == THREAD_STATUS_ACTIVE) {
        var threadBlock = $('div.thread').children('.post.dead_post').each(function() {
            $(this).removeClass('dead_post');
        });
    } else if (status == THREAD_STATUS_ARCHIVE) {
        var threadBlock = $('div.thread').children('.post:not(.archive_post)').each(function() {
            $(this).removeClass('dead_post');
            $(this).addClass('archive_post');
        });
    }
    $('#rollover-link').attr('data-thread-status', status);
}

function completeThreadUpdate() {
    if (timeoutState != null) {
        clearTimeout(timeoutState);
        timeoutState = null;
    }
}

/**
 * Add or update the post on html page.
 */
function addOrUpdatePost(post) {
    var postId = post.attr('id');

    // If the post already exists, replace it. Otherwise add as a new one.
    var existingPosts = $('.thread > .post[id=' + postId + ']');

    var type;

    if (existingPosts.length > 0) {
        // Update post
        //replacePartial(existingPosts.first(), post, false);
        existingPosts.first().replaceWith(post);
        post = existingPosts.first();

        type = false;
    } else {
        // Add new post

        // This needs to be set on start because the page is scrolled after posts
        // are added or updated
        var bottom = isPageBottom();
        var postId = parseInt(post.attr('id'));
        var allPosts = $('.thread > .post');
        var previousPost = null;

        allPosts.each(function(i) {
            var curPost = $(this);
            var curPostId = parseInt(curPost.attr('id'));

            if (curPostId < postId) {
                previousPost = curPost;
            } else {
                return false;
            }
        });

        post.insertAfter(previousPost);

        if (bottom) {
            scrollToBottom();
        }

        type = true;
    }

    processNewPost(post);

    return type;
}

/**
 * Initiate a blinking animation on a node to show it was updated.
 */
function blink(node) {
    node.fadeOut(BLINK_SPEED).fadeIn(BLINK_SPEED);
}

function isPageBottom() {
    var scroll = $(window).scrollTop() / ($(document).height()
        - $(window).height());

    return scroll == 1
}

function scheduleThreadUpdate() {
    timeoutState = setTimeout(getThreadDiff, JS_AUTOUPDATE_PERIOD);
}

/**
 * Update bumplimit progress bar
 */
function updateBumplimitProgress(postLimit) {
    var progressBar = $('#bumplimit_progress');
    if (progressBar) {
        var fullness = Math.min(100.0, 1.0 * $(".thread").children(".post").length / postLimit * 100);
        progressBar.width((100 - fullness) + '%');
    }
}

/**
 * Show 'new posts' text in the title if the document is not visible to a user
 */
function showNewPostsTitle(newPostCount) {
    if (document.hidden) {
        if (documentOriginalTitle === '') {
            documentOriginalTitle = document.title;
        }
        unreadPosts = unreadPosts + newPostCount;

        var newTitle = null;
        if (unreadPosts == 0) {
            newTitle = '* ';
        } else {
            newTitle = '[' + unreadPosts + '] ';
        }
        newTitle += documentOriginalTitle;

        document.title = newTitle;

        document.addEventListener('visibilitychange', function() {
            if (documentOriginalTitle !== '') {
                document.title = documentOriginalTitle;
                documentOriginalTitle = '';
                unreadPosts = 0;
            }

            document.removeEventListener('visibilitychange', null);
        });
    }
}

/**
 * Run js methods that are usually run on the document, on the new post
 */
function processNewPost(post) {
    addScriptsToPost(post);
    blink(post);
}

function replacePartial(oldNode, newNode, recursive) {
    if (!equalNodes(oldNode, newNode)) {
        // Update parent node attributes
        updateNodeAttr(oldNode, newNode, ATTR_CLASS);
        updateNodeAttr(oldNode, newNode, ATTR_UUID);

        // Replace children
        var children = oldNode.children();
        if (children.length == 0) {
            oldNode.replaceWith(newNode);
        } else {
            var newChildren = newNode.children();
            newChildren.each(function(i) {
                var newChild = newChildren.eq(i);
                var newChildClass = newChild.attr(ATTR_CLASS);

                // Update only certain allowed blocks (e.g. not images)
                if (ALLOWED_FOR_PARTIAL_UPDATE.indexOf(newChildClass) > -1) {
                    var oldChild = oldNode.children('.' + newChildClass);

                    if (oldChild.length == 0) {
                        oldNode.append(newChild);
                    } else {
                        if (!equalNodes(oldChild, newChild)) {
                            if (recursive) {
                                replacePartial(oldChild, newChild, false);
                            } else {
                                oldChild.replaceWith(newChild);
                            }
                        }
                    }
                }
            });
        }
    }
}

/**
 * Compare nodes by content
 */
function equalNodes(node1, node2) {
    return node1[0].outerHTML == node2[0].outerHTML;
}

/**
 * Update attribute of a node if it has changed
 */
function updateNodeAttr(oldNode, newNode, attrName) {
    var oldAttr = oldNode.attr(attrName);
    var newAttr = newNode.attr(attrName);
    if (oldAttr !== newAttr) {
        oldNode.attr(attrName, newAttr);
    }
}
