/*
 @licstart  The following is the entire license notice for the
 JavaScript code in this page.


 Copyright (C) 2013  neko259

 The JavaScript code in this page is free software: you can
 redistribute it and/or modify it under the terms of the GNU
 General Public License (GNU GPL) as published by the Free Software
 Foundation, either version 3 of the License, or (at your option)
 any later version.  The code is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

 As additional permission under GNU GPL version 3 section 7, you
 may distribute non-source (e.g., minimized or compacted) forms of
 that code without the copy of the GNU GPL normally required by
 section 4, provided you include this license notice and a URL
 through which recipients can access the Corresponding Source.

 @licend  The above is the entire license notice
 for the JavaScript code in this page.
 */

var ITEM_VOLUME_LEVEL = 'volumeLevel';
var PROP_VOLUME = "volume";
var SELECTOR_VOLUME_AWARE = "video,audio";

var ITEM_HIDDEN_POSTS = 'hiddenPosts';


var REGEX_PAGE = new RegExp("page\=\\d+", "g");
var ATTR_PAGE = "page=";

var IMAGE_TYPES = ['image/png', 'image/jpg', 'image/jpeg', 'image/bmp', 'image/gif'];

var CONTENT_TYPE_JSON = 'application/json; charset=utf-8';

var PAGINATOR_LOOKAROUND = 3;

var TARGET_DELIMITER = "#";

var compiledTemplates = {};
var singleTemplates = ["active-threads"]

var fileTemplate = "<img class='url-image' src='${url}' width='${width}' height='${height}'>";
var genericAttachmentTemplate = "<div class='image'><a ${linkAttrs} href='${fileUrl}'>${formatView}</a><div class='image-metadata'>${metadata}</div></div>";
var attachmentTemplates = {
    "audio": "<audio controls preload='metadata' src='${fileUrl}'></audio>",
    "file": fileTemplate,
    "missing": fileTemplate,
    "url": fileTemplate,
    "image": "<img class='post-image-preview' src='${thumbUrl}'"
                     + " alt='${id}' height='${thumbHeight}' width='${thumbWidth}' data-width='${width}' data-height='${height}' data-orientation='${orientation}' />",
    "vector": "<img class='post-image-preview' src='${url}'"
              			+ " alt='${id}' height='${height}' width='${width}'/>",
    "video": "<video width='200' height='150' controls preload='metadata' src='${fileUrl}'></video>"
}

var target = "";
var targetPos = window.location.href.indexOf(TARGET_DELIMITER);
if (targetPos > 0) {
    target = window.location.href.substring(targetPos);
}

var currentUrl = location.href;

function gettext(key) {
    // TODO Figure out how to get localized message
    return key;
}

function renderTemplate(name, data) {
    var template = compiledTemplates[name];
    if (template == null) {
        template = Handlebars.compile($('#template-' + name).html());
        compiledTemplates[name] = template;
    }
    return template(data);
}

/**
 * Highlight code blocks with code highlighter
 */
function highlightCode(node) {
    node.find('pre code').each(function(i, e) {
//        hljs.highlightBlock(e);
    });
}

function processVolumeUser(node) {
    if (!window.localStorage) {
        return;
    }

    function setVolumeLevel(level) {
        localStorage.setItem(ITEM_VOLUME_LEVEL, level);
    }

    function getVolumeLevel() {
        var level = localStorage.getItem(ITEM_VOLUME_LEVEL);
        if (level == null) {
            level = 1.0;
        }
        return level
    }

    var volumeAware = node.find(SELECTOR_VOLUME_AWARE);
    volumeAware.prop(PROP_VOLUME, getVolumeLevel());
    volumeAware.on("volumechange", function(event) {
        var newLevel = event.target.volume;
        setVolumeLevel(newLevel);
        $(SELECTOR_VOLUME_AWARE).prop(PROP_VOLUME, newLevel);
    });
}

/**
 * Add all scripts than need to work on post, when the post is added to the
 * document.
 */
function addScriptsToPost(post) {
    addRefLinkPreview(post);
    highlightCode(post);
    processVolumeUser(post);

    if (typeof initAdminButtons !== 'undefined') {
    	initAdminButtons(post);
    }

    operaMiniDelegationFix(post);
}

/**
 * Fix compatibility issues with some rare browsers
 */
function compatibilityCrutches(form) {
    if (window.operamini) {
        form.find('textarea').each(function() { this.placeholder = ''; });
        form.find('.file-picker').hide();
        form.find('input[type="file"]').show();
    }
}

function operaMiniDelegationFix(post) {
    if (window.operamini) {
        // Because of the anchor link, Opera Mini's delegation detector
        // doesn't trigger there, so working this around with a dummy
        // handler
        post.find('.quick-reply').each(function() {
            $(this).on('click', function() { return true; });
        });
    }
}

function hashCode(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}

function createAndSubmitAdminActionForm(action, post) {
    var form = document.createElement("form");
    var postIdInput = document.createElement("input");

    form.method = "POST";
    form.action = action;

    postIdInput.value=post.attr('id');
    postIdInput.name="postId";
    postIdInput.type="hidden"
    form.appendChild(postIdInput);

    document.body.appendChild(form);

    form.submit();
}

function refreshPageData() {
    if (typeof getThreadDiff === "function") {
        getThreadDiff();
    } else {
        location.reload();
    }
}

function addAdminContextMenu() {
	$.contextMenu({
		selector: '.admin-post-context-menu',
		items: {
			"edit": {name: "Edit"},
			"sep1": "---------",
			"ban": {name: "Ban"},
			"sep2": "---------",
			"delete": {name: "Delete"},
			"purge": {name: "Purge"},
		},
		callback: function(key, options) {
            var post = $(this).parents('.post');
            var postId = post.attr('id');

			switch (key) {
				case 'edit':
                    location.href = (post.data('opening') ? '/admin/edit/thread/' : '/admin/edit/post/') + postId;
					break
				case 'delete':
				    if (confirm(gettext('Do you really want to delete this?'))) {
                        $.ajax({
                            type: "POST",
                            url: '/api/post/' + postId + '/delete',
                            success: function(data) {
                                if (data.url != null) {
                                    location.href = data.url;
                                } else {
                                    refreshPageData();
                                }
                            },
                            dataType: 'json'
                        });
				    }
				    break
				case 'ban':
				    if (confirm(gettext('Do you really want to ban the user?'))) {
				        var address = $(this).attr('title');
                        $.ajax({
                            type: "POST",
                            url: '/api/admin/ban/',
                            data: {
                                postId: postId,
                                address: address
                            },
                            success: function() {
                                alert('User ' + address + ' banned')
                            },
                            dataType: 'json'
                        });
				    }
				    break
				case 'purge':
				    if (confirm(gettext('Do you really want to ban the author and delete his posts earlier than this one?'))) {
                        $.ajax({
                            type: "POST",
                            url: '/api/post/purge/',
                            data: {
                                postId: postId
                            },
                            success: function() {
                                alert('User purged')
                            },
                            dataType: 'json'
                        });
    				}
				    break;
			}
		}
	});
}

/**
    Available options:
    - replyButton
    - openButton
    - in
*/

function scrollToTarget(resetTarget) {
    if (target) {
       $([document.documentElement, document.body]).animate({
            scrollTop: $(target).offset().top
       }, 500);
    }

    if (resetTarget) {
        target = "";
    }
}

function addApiButtonHandler(node) {
    node.find('button[data-url^="/api/"]').click(function(event) {
        var button = $(this);
        if (button.attr("data-confirm")) {
            if (!confirm(button.attr("data-confirm"))) {
                return;
            }
        }

        var url = button.attr("data-url");
        $.post(url)
            .done(function() {
                document.location.reload();
            });
        event.stopPropagation();
    });
}

function render(model, options, template) {
    var result = null;
    if (template == null) {
        template = model.type;
    }

    var handlers = {
        post: function(postModel, options) {
            var threadId = postModel.id;
            var threadTitle = "";
            if (postModel.thread) {
                threadId = postModel.thread;
                threadTitle = postModel.threadTitle;
            }

            var classes = "post";
            if (postModel.threadStatus == "BUMPLIMIT") {
                classes += " dead_post";
            } else if (postModel.threadStatus == "ARCHIVED") {
                classes += " archive_post";
            }

            var data = {
                class: classes,
                id: postModel.id,
                title: postModel.title,
                text: postModel.text,
                url: postModel.url,
                uuid: postModel.uuid,
                pubTime: new Date(postModel.pubTime).toLocaleString(),
                opening: postModel.opening,
                posterAddress: postModel.posterAddress,
                threadId: threadId,
                threadTitle: threadTitle
            };

            if (postModel.tripcode) {
                data["tripcodeColor"] = postModel.tripcode.substring(0, 6);
                data["tripcode"] = postModel.tripcode;
            }

            if (postModel.posterAddress) {
                data["posterAddressColor"] = intToRGB(hashCode(postModel.posterAddress));
            }

            if (postModel.replies) {
                var refmap = [];
                $.each(postModel.replies, function(index, element) {
                    var replyUrl = "<a href='" + element.url + "'>&gt;&gt;" + element.id + "</a>";
                    if (element.opening) {
                        replyUrl = "<b>" + replyUrl + "</b>";
                    }
                    refmap.push(replyUrl);
                });
                data["refmap"] = refmap.join(', ');
            }

            var attachments = [];
            if (postModel.attachments && postModel.attachments.length > 0) {
                $.each(postModel.attachments, function(index, element) {
                    attachments.push(element.attachment);
                });
            }
            data["attachments"] = attachments;

            if (postModel.opening) {
                var tags = [];
                $.each(postModel.tags, function(index, element) {
                    tags.push("<a class='tag' href='/tag/" + element + "'>" + element + "</a>")
                });

                var threadCounters = "";
                if (options != null && options.indexOf("threadCounters") > -1) {
                    threadCounters = "&heartsuit; " + postModel.postCount + " &#x2744; "
                        + postModel.attachmentCount + " ";
                }
                data["metadata"] = threadCounters + tags.join(", ");
            }
            data["replies"] = postModel.replies && postModel.replies.length > 0;
            data["admin"] = postModel.posterAddress != null;
            data["active"] = postModel.status != "ARCHIVED";

            if (options) {
                data["replyButton"] = options.indexOf("replyButton") > -1;
                data["openButton"] = options.indexOf("openButton") > -1;
                data["threadUrl"] = options.indexOf("threadUrl") > -1;
            }

            var newPost = renderTemplate("post", data);

            var renderedPost = $(newPost);

            addScriptsToPost(renderedPost);
            return renderedPost;
        },
        tag: function(tagModel, options) {
            var result = renderTemplate("tag", {
                name: tagModel.name
            });
            return $(result);
        },
        thread: function(threadModel, options) {
            var fullness = Math.min(100.0, 1.0 * threadModel.posts.length / threadModel.postLimit * 100);
            var emptyness = 100 - fullness;
            var result = renderTemplate("thread", {
                id: threadModel.id,
                status: threadModel.status,
                title: threadModel.title,
                emptyness: emptyness,
                posts: threadModel.posts,
                options: options
            });

            var thread = $(result);
            var threadBlock = $(thread.get(8));

            threadBlock.on('click', '.quick-reply', function() {
                addQuickReply($(this).parents('.post').attr('id'));
                return false;
            });
            $(thread.get(4)).click(function() {
                addQuickQuote();
            })

            addScriptsToPost(thread.find(".post"));

            if (threadModel.status !== "ARCHIVED") {
                initThread();
                scheduleThreadUpdate();
            }

            return thread;
        },
        "tag-info": function(tagModel, options) {
            var result = renderTemplate("tag-info", {
                name: tagModel.name,
                favorite: tagModel.favorite,
                hidden: tagModel.hidden
            });
            result = $(result);

            addApiButtonHandler(result);

            return result;
        },
        "thread-preview": function(threadModel, options) {
            var result = renderTemplate("thread-preview", {
                threadId: threadModel.openingPost.id,
                openingPost: threadModel.openingPost,
                skippedReplies: threadModel.skippedPosts,
                lastPosts: threadModel.lastPosts,
                options: options
            });

            var thread = $(result);

            addScriptsToPost(thread.find(".post"));

            return thread;
        },
        "active-threads": function(model, options) {
            var result = renderTemplate("active-threads", {
                models: model.models
            });
            result = $(result);
            addRefLinkPreview(result);
            return result;
        },
        form: function(model, options) {
            var result = renderTemplate("form", {
                formTitle: model.formTitle,
                title: model.title,
                action: model.action,
                edit: model.edit,
                tags: model.tags,
                text: model.text,
                tagsEnabled: model.tagsEnabled
            });
            result = $(result);
            addRefLinkPreview(result);

            var form = result.find('.post-form');

            addOnImagePaste(form);
            addOnFileAdded(form);
            addOnLinkPaste(form);
            addOnDrag(form);

            enableStickerAutocomplete(form);
            enableTagsAutocomplete(form);
            initPreviewButton(form);
            initPanelButtons(form);
            enableSubmitHotkey(form);
            compatibilityCrutches(form);

            initAjaxForm(form.find("form"));

            return result;
        },
        "feed-criteria": function(model, options) {
            var result = renderTemplate("feed-criteria", {
                key: model.key,
                value: model.value
            });
            return $(result);
        },
        "search-form": function(model, options) {
            var result = renderTemplate("search-form", {
                query: model.query,
                admin: model.admin
            });
            result = $(result);
            if (model.admin) {
                addApiButtonHandler(result);
            }
            return result;
        },
        gallery: function(model, options) {
            var result = renderTemplate("gallery", {
                id: model.id,
                attachments: model.attachments
            });

            return  $(result);
        },
        attachment: function(model, options) {
            var attachmentType = model.viewerType;
            var formatView = attachmentTemplates[attachmentType];
            var str = genericAttachmentTemplate.replace('${formatView}', formatView);

            var data = model.data;
            $.each(data, function(key, value) {
                str = str.replace('${' + key + '}', value);
            });

            var result = renderTemplate("attachment", {
                content: str
            });
            return $(result);
        }
    }
    return handlers[template](model, options);
}

function loadModelsAsync(block, replaceUrl) {
    function getPageQuery(pageNumber) {
        var queryPos = window.location.href.indexOf("?");
        if (queryPos > -1) {
            var queryString = window.location.href.substring(queryPos);
            if (queryString.indexOf(ATTR_PAGE) > -1) {
                queryString = queryString.replace(REGEX_PAGE, ATTR_PAGE + pageNumber);
            } else {
                queryString += "&" + ATTR_PAGE + pageNumber;
            }
        } else {
            queryString = "?" + ATTR_PAGE + pageNumber;
        }
        return queryString
    }

    function getPaginator(webUrl, apiUrl, pageNumber, totalPages, options) {
        var pageNumbers = [];

        var leftLimit = Math.min(PAGINATOR_LOOKAROUND, totalPages);
        var rightLimit = Math.max(0, totalPages - PAGINATOR_LOOKAROUND);

        for (var i = 0; i < leftLimit; i++) {
            if (pageNumbers.indexOf(i) == -1) {
                pageNumbers.push(i);
            }
        }

        for (var i = pageNumber - PAGINATOR_LOOKAROUND + 1; i < pageNumber + PAGINATOR_LOOKAROUND && i > 0 && i < totalPages; i++) {
            if (pageNumbers.indexOf(i) == -1) {
                pageNumbers.push(i);
            }
        }

        for (var i = rightLimit; i < totalPages; i++) {
            if (pageNumbers.indexOf(i) == -1) {
                pageNumbers.push(i);
            }
        }

        var pageUrls = [];
        for (var i = 0; i < pageNumbers.length; i++) {
            var tempPage = pageNumbers[i];
            var cls = "";
            if (pageNumber == tempPage) {
                cls = "class='current_page'";
            }
            var query = getPageQuery(tempPage);
            var optionsAttr = "";
            if (options) {
                optionsAttr = "data-options='" + options.join(" ") + "'";
            }
            pageUrls.push("<a data-page-link='true'" + cls + " href='"
                + webUrl + query + "' data-async='"
                + apiUrl + query + "' " + optionsAttr
                + " >" + tempPage + "</a>");
        }

        return "[ " + pageUrls.join(", ") + " ]";
    }

    function isEverythingLoaded() {
        return $("div[data-async]:not([data-loaded])").length == 0;
    }

    var options = [];
    if (block.attr("data-options")) {
        options = block.attr("data-options").split(" ");
    }

    var modelsContainer = block;
    if (block.attr("data-page-link")) {
        modelsContainer = $("[data-pageable='true']");
    }
    modelsContainer.empty();

    $(renderTemplate("loading", null)).appendTo(modelsContainer);

    var template = block.attr("data-template");

    $.ajax({
        type: "GET",
        url: block.attr('data-async'),
        contentType: CONTENT_TYPE_JSON,
        success: function(data) {
            var models = data.models;

            modelsContainer.empty();
            if (template != null && singleTemplates.indexOf(template) > -1) {
                render(data, options, template).appendTo(modelsContainer);
            } else {
                $.each(models, function(index, element) {
                    render(element, options, template).appendTo(modelsContainer);
                });
            }

            if (data.webUrl) {
                var prevUrlBlock = $("#previous-page-url");
                if (data.pageNumber > 0) {
                    var pageQuery = getPageQuery(data.pageNumber - 1);
                    prevUrlBlock.attr("data-async", data.apiUrl + pageQuery);
                    prevUrlBlock.attr("href", data.webUrl + pageQuery);
                } else {
                    prevUrlBlock.removeAttr("data-async");
                }
                var nextUrlBlock = $("#next-page-url")
                if (data.pageNumber < data.pages - 1) {
                    var pageQuery = getPageQuery(data.pageNumber + 1);
                    nextUrlBlock.attr("data-async", data.apiUrl + pageQuery);
                    nextUrlBlock.attr("href", data.webUrl + pageQuery);
                } else {
                    nextUrlBlock.removeAttr("data-async");
                }
                $("#paginator").html(getPaginator(data.webUrl, data.apiUrl,
                    data.pageNumber, data.pages, options));
                $("#paginator").find("a[data-page-link]").click(function(event) {
                    event.preventDefault();
                    loadModelsAsync($(this), $(this).attr("href"));
                });
            }

            if (data.title) {
                document.title = data.title + " - " + $(".navigation_panel").attr("data-site-name");
            }

            modelsContainer.attr("data-loaded", true);

            if (isEverythingLoaded()) {
                scrollToTarget(true);
            }

            if (replaceUrl) {
                currentUrl = replaceUrl;
                window.history.pushState(null, document.title, replaceUrl);
            }
        }
    });
}

$( document ).ready(function() {
    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    // TODO These two should be added only when posts
    // are loaded, since there are no more pages where
    // posts are loaded at page start
    addImgPreview();

    addAdminContextMenu();

    Handlebars.registerHelper("render", function(context, options) {
        return render(this, context).wrap("<div/>").parent().html();
    });

    var blocks = $("[data-async]");
    blocks.each(function() {
        loadModelsAsync($(this));
    });

    $("a[data-page-link]").click(function(event) {
        event.preventDefault();
        loadModelsAsync($(this), $(this).attr("href"));
    });

    $(window).on("popstate", function (e) {
        var newUrl = e.target.location.href;

        var newUrlTargetLocation = newUrl.indexOf(TARGET_DELIMITER);
        var newUrlNoTarget = newUrl;
        if (newUrlTargetLocation > -1) {
            newUrlNoTarget = newUrlNoTarget.substring(0, newUrlTargetLocation);
        }

        var currentUrlTargetLocation = currentUrl.indexOf(TARGET_DELIMITER);
        var currentUrlNoTarget = currentUrl;
        if (currentUrlTargetLocation > -1) {
            currentUrlNoTarget = currentUrlNoTarget.substring(0, currentUrlTargetLocation);
        }

        if (newUrlNoTarget !== currentUrlNoTarget) {
            location.reload();
        }
    });
});
