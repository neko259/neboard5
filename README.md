# Depends
* Java 9 or later should be installed. Tested on up to openjdk 12.

# Running
Configure server parameters in src/main/resources/application.properties

Don't forget to configure and run external parser service and image service in
application properties.
You won't be able to create posts without it. It can be either one public shared
service, or your private service (it's completely stateless so it does not matter).

Configure database properties in application.properties and drivers in build.gradle.
It's configured to test h2 by default. Migrations are applied when the application
is run.

To run locally by gradle:
1. Run "./gradlew bootRun" to start the server. It will be running on port 8000 by default.

To run in Docker
1. Run "./gradlew build" to build the project.
2. Run "docker build ." to build the image. Port 8000 should be forwarded.
3. When running, add volume for your database if needed

# Donations
If you like this board and wish to contribute, please send bitcoins to **12wpULLdtgT7VfjNVA2pdZCY9SB63gPVdU**