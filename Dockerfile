FROM openjdk:17-slim as build
COPY . .
RUN ./gradlew build

FROM openjdk:17-slim
EXPOSE 8000
COPY --from=build ./build/libs/*-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]