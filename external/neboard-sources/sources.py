import calendar
import json
import logging
from datetime import datetime
import dateutil.parser
import time
import os

import feedparser
import requests

SOURCE_TYPE_RSS = 'rss'


def check_sources():
    with open('settings.json', 'r') as s:
        sources = json.load(s)

    with open("timestamps.json", "r") as f:
        timestamps = json.load(f)

    for source in sources:
        source_url = source['sourceUrl']
        source_name = source['name']
        source_type = source['type']

        thread = source['boardThread']

        api_key = source['boardApiKey']
        tripcode = source['tripcode']

        board_url = source['boardUrl']

        start_timestamp = timestamps.get(source_name)
        if start_timestamp:
            start_timestamp = dateutil.parser.parse(start_timestamp)

        last_timestamp = start_timestamp

        headers = {
            'X-API-SECRET': api_key
        }

        logging.basicConfig(level=logging.INFO)
        logging.info('Start timestamp is {}'.format(start_timestamp))

        if SOURCE_TYPE_RSS == source_type:
            feed = feedparser.parse(source_url)
            items = sorted(feed.entries, key=lambda entry: entry.published_parsed)
            logging.info('Found {} items in feed'.format(len(items)))

            last_timestamp = None
            last_added_timestamp = None
            for item in items:
                title = item.title  # TODO Ignore max length for now, let the server handle this
                timestamp = datetime.fromtimestamp(calendar.timegm(item.published_parsed))
                if not timestamp:
                    logging.error('Invalid timestamp {} for {}'.format(item.published, title))
                else:
                    if last_timestamp is None or timestamp > last_timestamp:
                        last_timestamp = timestamp
                    if (start_timestamp is None) or (timestamp > start_timestamp):
                        data = {
                            'title': title,
                            'text': item.description,
                            'thread': thread,
                            'urls': [item.link],
                            'tripcode': tripcode
                        }
                        resp = requests.post(board_url + '/api/post/service/add-post', json=data, headers=headers)
                        if resp.status_code == 200:
                            logging.info('Fetched item {} from {} into thread {}'.format(
                                title, source_name, thread))
                            last_added_timestamp = last_timestamp
                        else:
                            logging.error('Cannot write into thread {}'.format(thread))
                            logging.error(resp.text)
                            break

            if last_added_timestamp is not None:
                logging.info('New timestamp for {} is {}'.format(source_name, last_added_timestamp))
                timestamps[source_name] = datetime.isoformat(last_added_timestamp)
        else:
            logging.warning('Unknown source type {}'.format(source_type))

    with open('timestamps.json', 'w') as f:
        json.dump(timestamps, f)


period = float(os.environ['CHECK_PERIOD_SECONDS'])

start_time = time.time()
while True:
    check_sources()
    time.sleep(period - ((time.time() - start_time) % period))
